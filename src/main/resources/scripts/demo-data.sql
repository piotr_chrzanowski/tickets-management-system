
INSERT INTO user_details (username, password, roles, active, first_name, last_name, department, phone, email)
VALUES          ('DEMO_USER', '$2a$12$0Y25tyMRfx1ipeHVnazM7.XnjQ/t98QiOm0cSwISKFzanfd9khwSu', 'ROLE_USER', '1', 'DEMO_USER', 'DEMO_USER', 'DEMO_USER', '+48777111111', 'DEMO_USER@testmail.com'),
                ('DEMO_ADMIN', '$2a$12$A2I1tgxwovSEe27C/pO54ucS4Ol4gErQP1Y75hyBM/CLcyaLy3Sha', 'ROLE_USER,ROLE_ADMIN', '1', 'DEMO_ADMIN', 'DEMO_ADMIN', 'DEMO', '+48777111112', 'DEMO_ADMIN@testmail.com'),
                ('user', '$2a$12$Ujsc4yXryBg7a3KEVUVITeC.REREe35ejqvePPnhbpjbfi6sBjkQ.', 'ROLE_USER', '1', 'James', 'Smith', 'IT', '+48777111113', 'jamessmith@testmail.com'),
                ('user1', '$2a$12$N6NodhF8S8OMOBdp96WeveAalNPPNrRtMXw0V9fR4zjrLBJIwUYOO', 'ROLE_USER', '1', 'Michael', 'Smith', 'IT', '+48777111114', 'michaelsmith@testmail.com'),
        		('user2', '$2a$12$eRAVhGluLeWyBX8lOz5gHuYYZdeAP.Q2JWo/JwB.1TwMVB8kh81fW', 'ROLE_USER', '1', 'George', 'Brown', 'IT', '+48777111115', 'gorgebrown@testmail.com'),
        		('user3', '$2a$12$4eSlFsOedv0hFWA3hRkLKOtbtFQu6dYTPsgm5s8/LbFSZOfLw3nMa', 'ROLE_USER', '1', 'Robert', 'Miller', 'IT', '+48777111116', 'robertmiller@testmail.com'),
        		('user4', '$2a$12$/aNhTzPlLfhPwxZxmuXnnei1jV8jzoBgInB3Le0aatyKth0gx9dK.', 'ROLE_USER', '1', 'Maria', 'Garcia', 'Sales', '+48777111117', 'mariagarcia@testmail.com'),
        		('user5', '$2a$12$.hcERwQYo1AHSFDq9tqikuZYfmCwsxeqpqiJ.YPzErtnNEAUoiWwq', 'ROLE_USER', '1', 'Maria', 'Rodriguez', 'Sales', '+48777111118', 'mariarodriguez@testmail.com'),
        		('user6', '$2a$12$Cdg.5K/GJsxOtNeS5ux9LOafEy2/4gHQXaoqAbYQd0wwEL4ukYefe', 'ROLE_USER', '1', 'Ann', 'Hernandez', 'Sales', '+48777111119', 'annhernandez@testmail.com'),
        		('user7', '$2a$12$R71mQ9hbt4Q0SufnFSd79eYOOJaCuvpFxv0f/rj1uJ09BPmNyY84S', 'ROLE_USER', '1', 'James', 'Johnson', 'Service', '+48777111121', 'jamesjohnson@testmail.com'),
        		('user8', '$2a$12$c6xU9T4yzBzUVBzKYvbzKuVkN4kaeRO/tD5HK7tZKmA6JKSUd4hUi', 'ROLE_USER', '1', 'Michael', 'Davis', 'Service', '+48777111122', 'michaeldavis@testmail.com'),
        		('user9', '$2a$12$LfaQZ67vLSToVcj2OCtVhu71N8v1sg61pe6/tVkUT0h8gRFY/II6a', 'ROLE_USER', '1', 'William', 'Clark', 'Service', '+48777111123', 'williamclark@testmail.com'),
                ('admin', '$2a$12$50oh1x.pUaPzmSxGKhnmf./wIZphfVCHgVev7SBjj4bP9xUtbJAVS', 'ROLE_USER,ROLE_ADMIN', '1', 'Thomas', 'Taylor', 'IT', '+48777111124', 'thomastaylor@testmail.com');


INSERT INTO ticket (title, status, priority, category, last_updated, created,  requester_id)
VALUES  ('Title 0', 'Open', 'Low', 'Software', PARSEDATETIME('18:47:52 17-09-2012','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('18:47:52 17-09-2012','HH:mm:ss dd-MM-yyyy'), 3),
        ('Title 1', 'Closed', 'Low', 'Software', PARSEDATETIME('18:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('17:47:52 17-06-2022','HH:mm:ss dd-MM-yyyy'), 3),
        ('Title 2', 'New', 'Low', 'Software', PARSEDATETIME('05:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('04:47:52 17-05-2022','HH:mm:ss dd-MM-yyyy'), 4),
        ('Title 3', 'Open', 'Low', 'Software', PARSEDATETIME('02:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('19:47:52 17-05-2022','HH:mm:ss dd-MM-yyyy'), 5),
        ('Title 4', 'Closed', 'Low', 'Software', PARSEDATETIME('19:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('19:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), 6),
        ('Title 5', 'Open', 'Low', 'Software', PARSEDATETIME('22:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('22:47:52 17-11-2022','HH:mm:ss dd-MM-yyyy'), 7),
        ('Title 6', 'Open', 'Low', 'Software', PARSEDATETIME('23:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('23:47:52 17-12-2022','HH:mm:ss dd-MM-yyyy'), 8),
        ('Title 7', 'Open', 'Low', 'Software', PARSEDATETIME('23:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('23:47:52 17-12-2022','HH:mm:ss dd-MM-yyyy'), 1),
        ('Title 8', 'Open', 'Low', 'Software', PARSEDATETIME('23:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('23:47:52 17-12-2022','HH:mm:ss dd-MM-yyyy'), 1),
        ('Title 9', 'Open', 'High', 'Software', PARSEDATETIME('23:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('23:47:52 17-12-2022','HH:mm:ss dd-MM-yyyy'), 2),
        ('Title 10', 'Open', 'Medium', 'Software', PARSEDATETIME('23:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('23:47:52 17-12-2022','HH:mm:ss dd-MM-yyyy'), 2),
        ('Title 11', 'Closed', 'Low', 'Web', PARSEDATETIME('23:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('23:47:52 17-12-2022','HH:mm:ss dd-MM-yyyy'), 1),
        ('Title 12', 'New', 'Low', 'Hardware', PARSEDATETIME('01:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('01:47:52 17-12-2022','HH:mm:ss dd-MM-yyyy'), 2),
        ('Title 13', 'New', 'Low', 'Hardware', PARSEDATETIME('01:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('01:47:52 17-12-2022','HH:mm:ss dd-MM-yyyy'), 2),
        ('Title 14', 'New', 'Low', 'Web', PARSEDATETIME('01:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('01:47:52 17-12-2022','HH:mm:ss dd-MM-yyyy'), 2),
        ('Title 15', 'New', 'Low', 'Web', PARSEDATETIME('01:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('01:47:52 17-12-2022','HH:mm:ss dd-MM-yyyy'), 2),
        ('Title 16', 'New', 'Low', 'Web', PARSEDATETIME('01:47:52 17-09-2022','HH:mm:ss dd-MM-yyyy'), PARSEDATETIME('01:47:52 17-12-2022','HH:mm:ss dd-MM-yyyy'), 2);


INSERT INTO ticket_details (description, ticket_id)
VALUES  ('Test ticket ', 1),
        ('Test ticket ', 2),
        ('Test ticket ', 3),
        ('Test ticket ', 4),
        ('Test ticket ', 5),
        ('Test ticket ', 6),
        ('Test ticket ', 7),
        ('Test ticket ', 8),
        ('Test ticket ', 9),
        ('Test ticket ', 10),
        ('Test ticket ', 11),
        ('Test ticket ', 12),
        ('Test ticket ', 13),
        ('Test ticket ', 14),
        ('Test ticket ', 15),
        ('Test ticket ', 16),
        ('Test ticket ', 17);








