DROP ALL OBJECTS;

CREATE TABLE user_details (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username varchar(68) NOT NULL,
    password varchar(68) NOT NULL,
    roles varchar(48) NOT NULL,
    active TINYINT NOT NULL,
    first_name varchar(48) DEFAULT NULL,
    last_name varchar(48) DEFAULT NULL,
    department varchar(48) DEFAULT NULL,
    phone varchar(48) DEFAULT NULL,
    email varchar(48) DEFAULT NULL
);


CREATE TABLE ticket (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title varchar(255) DEFAULT NULL,
    status varchar(255) DEFAULT NULL,
    priority varchar(255) DEFAULT NULL,
    category varchar(255) DEFAULT NULL,
    last_updated TIMESTAMP DEFAULT NULL,
    created TIMESTAMP DEFAULT NULL,
    requester_id int,

    FOREIGN KEY (requester_id) REFERENCES user_details(id)
);


CREATE TABLE ticket_details (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    description varchar(2000) DEFAULT NULL,
    ticket_id int,

    FOREIGN KEY (ticket_id) REFERENCES ticket(id)
);


CREATE TABLE comment (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    description varchar(300) DEFAULT NULL,
    created TIMESTAMP DEFAULT NULL,
    author_id int,
    ticket_id int,

    FOREIGN KEY (author_id) REFERENCES user_details(id),
    FOREIGN KEY (ticket_id) REFERENCES ticket(id)
);