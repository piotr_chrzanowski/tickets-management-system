$(function(){

    console.log(openedTickets);
    console.log(newTickets);
    console.log(closedTickets);

    Highcharts.chart('pie-chart', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: 'Tickets status'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
          }
        }
      },
      series: [{
        name: 'Amount',
        colorByPoint: true,
        data: [{
          name: 'New',
          y: newTickets,
          sliced: true,
          selected: true
        },
        {
          name: 'Opened',
          y: openedTickets
        },
        {
          name: 'Closed',
          y: closedTickets
        }]
      }]
    });
});