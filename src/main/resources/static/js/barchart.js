$(function(){

    Highcharts.chart('bar-chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Tickets created in last 12 months'
        },

        xAxis: {
            categories: ticketsPerMonthY,
            crosshair: true,
            title: {
                text: 'Month'
            }
        },
        yAxis: {
            min: 0,
            max: maxTicketNumber + 1,
            title: {
                text: 'Tickets number'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Tickets',
            data: ticketsPerMonthX
        }]
      });
    });