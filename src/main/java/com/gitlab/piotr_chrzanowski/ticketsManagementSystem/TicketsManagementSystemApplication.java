package com.gitlab.piotr_chrzanowski.ticketsManagementSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class TicketsManagementSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketsManagementSystemApplication.class, args);
	}

}
