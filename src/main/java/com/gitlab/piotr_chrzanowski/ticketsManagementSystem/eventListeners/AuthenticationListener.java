package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.eventListeners;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.UserCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
@Profile(value = {"dev", "prod"})
public class AuthenticationListener implements ApplicationListener<AuthenticationSuccessEvent> {
    @Autowired
    DataSource dataSource;

    @Override
    public void onApplicationEvent(final AuthenticationSuccessEvent event) {

        if (event.getAuthentication().getPrincipal() instanceof UserCredentials) {

            SecurityContextHolder.getContext().setAuthentication(event.getAuthentication());
            UserCredentials userCredentials = (UserCredentials) event.getAuthentication().getPrincipal();

            if (userCredentials.getUsername().contains("DEMO")) {
                String prefix = "demo";
                Resource initSchema = new ClassPathResource("scripts/" + prefix + "-schema.sql");
                Resource initData = new ClassPathResource("scripts/" + prefix + "-data.sql");
                DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initSchema, initData);
                DatabasePopulatorUtils.execute(databasePopulator, dataSource);
            }
        }
    }
}