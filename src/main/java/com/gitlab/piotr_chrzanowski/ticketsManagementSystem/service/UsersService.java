package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UsersService extends UserDetailsService {
    Page<User> findAll(Pageable page);
    Page<User> findByName(String name, Pageable page);
    User findById(int id);
    User findByUsername(String Username);
    public User save(User user);
    boolean deleteAll();
    boolean deleteById(int id);
    boolean validateUserPrivileges(String username);
}
