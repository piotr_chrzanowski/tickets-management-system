package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service;

import java.util.LinkedHashMap;
import java.util.Map;

public interface StatisticsService {
    public LinkedHashMap<String, Integer> calculateCreatedTicketNumberPerMonth();
    public Map<String, Double>  calculateTicketNumberPerStatus();
}
