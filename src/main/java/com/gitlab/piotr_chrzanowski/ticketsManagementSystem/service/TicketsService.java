package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Comment;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TicketsService {
    Page<Ticket> findByTitleContaining(String title, Pageable page);
    Page<Ticket> findAll(Pageable page);
    Page<Ticket> findAllForRequester(String username, Pageable page);
    Ticket findById(int id);
    Ticket save(Ticket ticket);
    boolean deleteAll();
    boolean deleteById(int id);
    Comment saveComment(Comment comment);
    boolean validateAuthorPrivileges(Ticket ticket);
}
