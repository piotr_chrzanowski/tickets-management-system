package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.UserCredentials;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository.UsersRepository;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {
    private UsersRepository usersRepository;

    private final PasswordEncoder passwordEncoder;


    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository, PasswordEncoder passwordEncoder) {
        this.usersRepository = usersRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Page<User> findAll(Pageable page) {
        return usersRepository.findAll(page);
    }

    @Override
    public Page<User> findByName(String name, Pageable page) {

        if (name.contains(" ")) {
            String[] fullName = name.split(" ");
            return usersRepository.findByFirstNameContainingOrLastNameContainingIgnoreCase(fullName[0], fullName[1], page);
        } else {
            return usersRepository.findByFirstNameContainingOrLastNameContainingIgnoreCase(name, name, page);
        }

    }

    @Override
    public User findById(int id) {
        Optional<User> result = usersRepository.findById(id);

        if (result.isPresent()) {
            return result.get();
        }

        return null;
    }

    @Override
    public User findByUsername(String username) {
        Optional<User> user = usersRepository.findByUsername(username);

        if (user.isPresent()) {
            return user.get();
        }

        return null;

    }

    @Override
    public User save(User user) {

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return usersRepository.save(user);
    }

    @Override
    public boolean deleteAll() {
        usersRepository.deleteAll();
        return true;
    }

    @Override
    public boolean deleteById(int id) {
        usersRepository.deleteById(id);
        return true;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = usersRepository.findByUsername(username);

        if (user.isPresent()) {
            return user.map(UserCredentials::new).get();
        } else {
            throw new UsernameNotFoundException("User not found: " + username);
        }
    }


    @Override
    public boolean validateUserPrivileges(String username) {
        if (Utility.getLoggedUser().getRoles().contains("ADMIN") ||
                username.equals(Utility.getLoggedUser().getUsername())) {
            return true;
        }

        return false;
    }
}
