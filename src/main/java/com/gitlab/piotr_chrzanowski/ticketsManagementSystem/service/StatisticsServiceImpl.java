package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Ticket;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository.TicketsRepository;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository.UsersRepository;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.utils.StatisticsUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StatisticsServiceImpl implements StatisticsService {
    private TicketsRepository ticketsRepository;
    private UsersRepository usersRepository;

    @Autowired
    public StatisticsServiceImpl(TicketsRepository ticketsRepository, UsersRepository usersRepository) {
        this.ticketsRepository = ticketsRepository;
        this.usersRepository = usersRepository;
    }

    @Override
    public LinkedHashMap<String, Integer> calculateCreatedTicketNumberPerMonth() {

        LinkedHashMap<String, Integer> ticketsPerMonthList = new LinkedHashMap<>();
        LinkedList<StatisticsUtility.MonthAndYear> monthAndYearList = StatisticsUtility.getLastMonthAndYearList(12);

        for (int i = 0; i < monthAndYearList.size(); ++i) {
            int ticketsNumberPerMonth = ticketsRepository.findByMonthAndYearMatch(monthAndYearList.get(i).month, monthAndYearList.get(i).year);
            ticketsPerMonthList.put(StatisticsUtility.convertMonthToString(monthAndYearList.get(i).month), ticketsNumberPerMonth);
        }

        return ticketsPerMonthList;
    }

    @Override
    public Map<String, Double> calculateTicketNumberPerStatus() {
        Map<String, Double> results = new HashMap<>();

        List<Ticket> newTickets = ticketsRepository.findByStatus("New");
        List<Ticket> openedTickets = ticketsRepository.findByStatus("Open");
        List<Ticket> closedTickets = ticketsRepository.findByStatus("Closed");

        double sum = newTickets.size() + openedTickets.size() + closedTickets.size();

        results.put("New", (newTickets.size()/sum * 100.00));
        results.put("Open", (openedTickets.size()/sum * 100.00));
        results.put("Closed", (closedTickets.size()/sum * 100.00));

        return results;
    }

}
