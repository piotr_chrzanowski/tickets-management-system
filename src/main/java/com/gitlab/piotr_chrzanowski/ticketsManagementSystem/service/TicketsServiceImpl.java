package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Comment;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Ticket;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository.CommentsRepository;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository.TicketsRepository;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class TicketsServiceImpl implements TicketsService {

    private TicketsRepository ticketsRepository;

    private CommentsRepository commentsRepository;

    @Autowired
    public TicketsServiceImpl(TicketsRepository ticketsRepository, CommentsRepository commentsRepository) {
        this.ticketsRepository = ticketsRepository;
        this.commentsRepository = commentsRepository;
    }

    @Override
    public Page<Ticket> findAll(Pageable page) {
        Page<Ticket> results = ticketsRepository.findAll(page);
        return results;
    }

    @Override
    public Page<Ticket> findByTitleContaining(String title, Pageable page) {
        Page<Ticket> results = ticketsRepository.findByTitleContaining(title, page);
        return results;
    }

    @Override
    public Page<Ticket> findAllForRequester(String username, Pageable page) {
        Page<Ticket> results = ticketsRepository.findByRequesterUsername(username, page);
        return results;
    }


    @Override
    public Ticket findById(int id) {
        Optional<Ticket> result = ticketsRepository.findById(id);

        if (result.isPresent()) {
            return result.get();
        }

        return null;
    }

    @Override
    public Ticket save(Ticket ticket) {

        if (ticket.getCreated() == null) {
            ticket.setCreated(LocalDateTime.now());
        }

        if (ticket.getRequester() == null) {
            ticket.setRequester(Utility.getLoggedUser());
        }

        ticket.getTicketDetails().setTicket(ticket);
        ticket.setLastUpdated(LocalDateTime.now());

        return ticketsRepository.save(ticket);
    }

    @Override
    public boolean deleteAll() {
        ticketsRepository.deleteAll();
        return true;
    }

    @Override
    public boolean deleteById(int id) {
        ticketsRepository.deleteById(id);
        return true;
    }

    @Override
    public Comment saveComment(Comment comment) {
        comment.setAuthor(Utility.getLoggedUser());
        comment.setCreated(LocalDateTime.now());
        return commentsRepository.save(comment);
    }

    @Override
    public boolean validateAuthorPrivileges(Ticket ticket) {
        if (Utility.getLoggedUser().getRoles().contains("ADMIN") ||
                ticket.getRequester().getUsername().equals(Utility.getLoggedUser().getUsername())) {
            return true;
        }

        return false;
    }
}
