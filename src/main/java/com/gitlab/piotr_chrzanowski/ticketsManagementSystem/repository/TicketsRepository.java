package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketsRepository extends JpaRepository<Ticket, Integer> {

    Page<Ticket> findByTitleContaining(String title, Pageable page);
    Page<Ticket> findByRequesterUsername(String username, Pageable page);
    List<Ticket> findByStatus(String status);
    @Query(value = "SELECT COUNT(*) FROM ticket AS t WHERE MONTH(t.created) = ?1 and YEAR(t.created) = ?2", nativeQuery = true)
    Integer findByMonthAndYearMatch(int month, int year);

}
