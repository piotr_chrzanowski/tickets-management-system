package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String username);
    Page<User> findByFirstNameContainingOrLastNameContainingIgnoreCase(String firstName, String lastName, Pageable page);
}
