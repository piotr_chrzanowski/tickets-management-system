package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CommentsRepository extends JpaRepository<Comment, Integer> {
}
