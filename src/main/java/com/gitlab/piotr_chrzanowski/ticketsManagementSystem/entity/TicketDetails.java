package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "ticket_details")
public class TicketDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    int id;


    @NotNull(message = "Description is empty!")
    @Size(max = 2000, min=10, message = "Description has not valid length (10-2000)!")
    @Column(name = "description")
    private String description;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "ticket_id", referencedColumnName = "id")
    private Ticket ticket;

    public TicketDetails() {
    }

    public TicketDetails(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}
