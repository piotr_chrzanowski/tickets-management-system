package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.validators.UniqueUsername;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.List;

@Entity
@Table(name = "user_details")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @UniqueUsername
    @Column(name = "username")
    private String username;

    @NotEmpty(message = "Password empty!")
    @Column(name = "password")
    private String password;
    @Column(name = "active")
    private boolean active;
    @NotEmpty(message = "Roles empty!")
    @Column(name = "roles")
    private String roles;

    @NotEmpty(message = "First Name empty!")
    @Column(name = "first_name")
    private String firstName;

    @NotEmpty(message = "Last Name empty!")
    @Column(name = "last_name")
    private String lastName;

    @NotEmpty(message = "Department empty!")
    @Column(name = "department")
    private String department;

    @NotEmpty(message = "Phone empty!")
    @Pattern(regexp = "(?<!\\w)(\\(?(\\+|00)?48\\)?)?[ -]?\\d{3}[ -]?\\d{3}[ -]?\\d{3}(?!\\w)", message = "Phone number not valid!")
    @Column(name = "phone")
    private String phone;
    @Email(message = "Wrong email format!")
    @NotEmpty(message = "Email empty!")
    @Column(name ="email")
    private String email;

    @OneToMany(mappedBy = "requester", cascade = {CascadeType.ALL})
    private List<Ticket> tickets;

    @OneToMany(mappedBy = "author", cascade = {CascadeType.ALL})
    private List<Comment> comments;

    public User() {
    }

    public User(int id, String username, String password, boolean active, String roles, String firstName, String lastName, String department, String phone, String email) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.active = active;
        this.roles = roles;
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.phone = phone;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
