package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "ticket")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotEmpty(message = "Ticket empty!")
    @Size(min=5, max=128, message="Title has not valid length!")
    @Column(name = "title")
    private String title;

    @NotEmpty(message = "Status is empty!")
    @Column(name = "status")
    private String status;
    @NotEmpty(message = "Priority is empty!")
    @Column(name = "priority")
    private String priority;
    @NotEmpty(message = "Category is empty!")
    @Column(name = "category")
    private String category;
    @Column(name = "last_updated")
    private LocalDateTime lastUpdated;

    @Column(name = "created")
    private LocalDateTime created;

    @Valid
    @OneToOne(mappedBy = "ticket", cascade = {CascadeType.ALL})
    private TicketDetails ticketDetails;

    @ManyToOne
    @JoinColumn(name = "requester_id" , referencedColumnName = "id")
    private User requester;

    @OneToMany(mappedBy = "ticket")
    @OrderBy("id DESC")
    private List<Comment> comments;

    public Ticket() {


    }

    public Ticket(int id, String title, String status, String priority, String category) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.priority = priority;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public User getRequester() {
        return requester;
    }

    public void setRequester(User requester) {
        this.requester = requester;
    }

    public TicketDetails getTicketDetails() {
        return ticketDetails;
    }

    public void setTicketDetails(TicketDetails ticketDetails) {
        this.ticketDetails = ticketDetails;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        comments.add(comment);
    }
}
