package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.validators;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = UniqueUsernameValidator.class)
@Target( {ElementType.FIELD})
@Retention( value = RetentionPolicy.RUNTIME)
public @interface UniqueUsername {

    public String value() default "";

    public String message() default "Username already exist!";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default {};

}
