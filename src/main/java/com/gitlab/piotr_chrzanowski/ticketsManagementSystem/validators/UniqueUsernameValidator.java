package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.validators;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {
    private UsersService usersService;

    public UniqueUsernameValidator() {
    }

    @Autowired
    public UniqueUsernameValidator(UsersService usersService) {
        this.usersService = usersService;
    }


    @Override
    public void initialize(UniqueUsername constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {

        if (usersService != null && usersService.findByUsername(username) != null) {
            return false;
        }

        return true;
    }
}
