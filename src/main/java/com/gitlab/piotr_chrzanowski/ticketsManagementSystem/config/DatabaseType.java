package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config;

public enum DatabaseType {
    NORMAL_USER,
    DEMO_USER
}
