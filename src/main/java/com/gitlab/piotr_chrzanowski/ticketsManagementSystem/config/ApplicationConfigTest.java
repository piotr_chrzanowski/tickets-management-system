package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.UserCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.function.BiFunction;
import java.util.function.Supplier;

@Configuration
@Profile(value = "test")
public class ApplicationConfigTest {
    private Environment env;

    @Autowired
    public ApplicationConfigTest(Environment env) {
        this.env = env;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public Supplier<String> getLoggedUserId() {
        return () -> {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return Integer.toString(((UserCredentials) principal).getUser().getId());
        };
    }

    @Bean
    public BiFunction<String, String, String> replaceOrAddParam() {
        return (paramName, newValue) -> ServletUriComponentsBuilder.fromCurrentRequest()
                .replaceQueryParam(paramName, newValue)
                .toUriString();
    }

}
