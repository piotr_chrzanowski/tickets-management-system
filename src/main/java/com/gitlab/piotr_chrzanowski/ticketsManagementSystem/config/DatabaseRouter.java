package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.utils.Utility;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

@Profile(value = {"dev", "prod"})
public class DatabaseRouter extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {

        try {
            String currentlyLoggedUser = Utility.getLoggedUser().getUsername();

            if (currentlyLoggedUser.equals("DEMO_ADMIN") || currentlyLoggedUser.equals("DEMO_USER")) {
                return DatabaseType.DEMO_USER;
            } else {
                return DatabaseType.NORMAL_USER;
            }

        } catch (Exception e) {
            return DatabaseType.NORMAL_USER;
        }


    }
}
