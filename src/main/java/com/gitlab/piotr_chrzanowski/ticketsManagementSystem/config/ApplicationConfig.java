package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.UserCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;

@Configuration
@Profile(value = {"dev", "prod"})
public class ApplicationConfig {

    private Environment env;

    @Autowired
    public ApplicationConfig(Environment env) {
        this.env = env;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public Supplier<String> getLoggedUserId() {
        return () -> {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return Integer.toString(((UserCredentials) principal).getUser().getId());
        };
    }

    @Bean
    public BiFunction<String, String, String> replaceOrAddParam() {
        return (paramName, newValue) -> ServletUriComponentsBuilder.fromCurrentRequest()
                .replaceQueryParam(paramName, newValue)
                .toUriString();
    }


    @Bean
    public DataSource getMultipleDataSources() {
        Map<Object, Object> dataSources = new HashMap<>();

        DataSource normalUserDataSource = getDataSource("normal");
        DataSource demoUserDataSource = getDataSource("demo");
        initializeDataSource(demoUserDataSource, "demo");

        dataSources.put(DatabaseType.NORMAL_USER, normalUserDataSource);
        dataSources.put(DatabaseType.DEMO_USER, demoUserDataSource);

        DatabaseRouter databaseRouter = new DatabaseRouter();
        databaseRouter.setTargetDataSources(dataSources);
        databaseRouter.setDefaultTargetDataSource(normalUserDataSource);

        return databaseRouter;
    }


    private DataSource getDataSource(String prefix) {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(env.getProperty(prefix + ".datasource.driverClassName"));
        dataSourceBuilder.url(env.getProperty(prefix + ".datasource.url"));
        dataSourceBuilder.username(env.getProperty(prefix + ".datasource.username"));
        dataSourceBuilder.password(env.getProperty(prefix + ".datasource.password"));
        return  dataSourceBuilder.build();
    }

    private void initializeDataSource(DataSource dataSource, String configurationFilePrefix) {
        Resource initSchema = new ClassPathResource("scripts/" + configurationFilePrefix + "-schema.sql");
        Resource initData = new ClassPathResource("scripts/" + configurationFilePrefix + "-data.sql");
        DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initSchema, initData);
        DatabasePopulatorUtils.execute(databasePopulator, dataSource);
    }

}
