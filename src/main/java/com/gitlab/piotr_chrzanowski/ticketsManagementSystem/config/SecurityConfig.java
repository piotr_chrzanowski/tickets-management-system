package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@Profile(value = {"dev", "prod", "test"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private UsersService usersService;

    @Autowired
    public SecurityConfig(UsersService usersService) {
        this.usersService = usersService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
         auth.userDetailsService(usersService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/login/demo").permitAll()
                .antMatchers("/").hasAnyRole("USER", "ADMIN")
                .antMatchers("/users/create**").hasRole("ADMIN")
                .antMatchers("/users/saveNew").hasRole("ADMIN")
                .antMatchers("/users/saveCredential").hasAnyRole("USER","ADMIN")
                .antMatchers("/users/saveInfo").hasAnyRole("USER", "ADMIN")
                .antMatchers("/users/updateInfo").hasAnyRole("USER", "ADMIN")
                .antMatchers("/users/updateCredential").hasAnyRole("USER","ADMIN")

                .antMatchers("/users/profile**").hasAnyRole("USER","ADMIN")
                .antMatchers("/users/delete**").hasRole("ADMIN")
                .antMatchers("/tickets/update**").hasAnyRole("USER", "ADMIN")
                .antMatchers("/tickets/delete**").hasRole("ADMIN")
                .antMatchers("/tickets/details**").hasAnyRole("USER", "ADMIN")
                .antMatchers("/tickets/all**").hasRole("ADMIN")
                .antMatchers("/tickets/my**").hasAnyRole("USER", "ADMIN")
                .antMatchers("/statistics**").hasAnyRole("USER", "ADMIN")
                .antMatchers("/resources/static**").permitAll()
                .antMatchers("/resources/templates**").permitAll()

                .and().formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/authenticateTheUser")
                    .permitAll()
                .and().logout()
                    .logoutUrl("/logout")
                    .logoutSuccessUrl("/login?logout")
                    .invalidateHttpSession(true)
                    .permitAll()
                .and().exceptionHandling()
                    .accessDeniedPage("/access-denied");
    }

}
