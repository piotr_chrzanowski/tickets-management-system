package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.controller;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Comment;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Ticket;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.TicketsService;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@org.springframework.stereotype.Controller
@RequestMapping("/tickets")
public class TicketsController {
    TicketsService ticketsService;
    private final int elementsPerPage = 15;

    @Autowired
    public TicketsController(TicketsService ticketsService) {
        this.ticketsService = ticketsService;
    }

    @GetMapping("/all")
    public String listAllTickets(@RequestParam(name = "page", defaultValue = "1") Integer page,
                                 @RequestParam(defaultValue = "id,asc") String sort,
                                 Model model) {

        PageRequest pageRequest = Utility.parsePageRequest(page, sort, elementsPerPage);
        Page<Ticket> tickets = ticketsService.findAll(pageRequest);
        model.addAttribute("tickets", tickets);
        model.addAttribute("pageNumbers", Utility.getListOfPages(tickets.getTotalPages()));

        return "ticket-list";
    }

    @GetMapping(value = "/my")
    public String listUserTickets(@RequestParam(name = "page", defaultValue = "1") Integer page,
                                  @RequestParam(defaultValue = "id,asc") String sort,
                                  Model model) {

        PageRequest pageRequest = Utility.parsePageRequest(page, sort, elementsPerPage);
        Page<Ticket> tickets = ticketsService.findAllForRequester(Utility.getLoggedUser().getUsername(), pageRequest);
        model.addAttribute("tickets", tickets);
        model.addAttribute("pageNumbers", Utility.getListOfPages(tickets.getTotalPages()));

        return "ticket-list";
    }

    @GetMapping("/search")
    public String searchTicketByTitle(@RequestParam(name = "title") String title,
                                      @RequestParam(name = "page", defaultValue = "1") Integer page,
                                      @RequestParam(defaultValue = "id,asc") String sort,
                                      Model model) {

        PageRequest pageRequest = Utility.parsePageRequest(page, sort, elementsPerPage);
        Page<Ticket> tickets = ticketsService.findByTitleContaining(title, pageRequest);
        model.addAttribute("pageNumbers", Utility.getListOfPages(tickets.getTotalPages()));
        model.addAttribute("tickets", tickets);

        return "ticket-list";
    }


    @GetMapping("/details")
    public String ticketDetails(@RequestParam("id") int id, Model model) {
        Ticket ticket = ticketsService.findById(id);
        if (ticketsService.validateAuthorPrivileges(ticket)) {
            Comment emptyComment = new Comment();
            model.addAttribute("ticket", ticket);
            model.addAttribute("emptyComment", emptyComment);
            return "ticket-details";
        }

        return "redirect:/access-denied";
    }

    @GetMapping("/create")
    public String createTicket(Model model) {
        Ticket ticket = new Ticket();
        model.addAttribute("ticket", ticket);
        return "ticket-new";
    }

    @PostMapping("/save")
    public String saveTicket(@Valid @ModelAttribute("ticket") Ticket ticket, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "ticket-new";
        }

        ticketsService.save(ticket);
        return "redirect:/tickets/my";
    }

    @PostMapping("/saveComment")
    public String saveTicket(@ModelAttribute("emptyComment") Comment comment,
                             @RequestParam("ticket_id") int id) {
        Ticket ticket = ticketsService.findById(id);
        comment.setTicket(ticket);
        ticketsService.saveComment(comment);
        return "redirect:/tickets/details?id=" + String.valueOf(comment.getTicket().getId());
    }

    @GetMapping("/update")
    public String updateTicket(@RequestParam("id") int id, Model model) {
        Ticket ticket = ticketsService.findById(id);

        if (ticketsService.validateAuthorPrivileges(ticket)) {
            model.addAttribute("ticket", ticket);
            return "ticket-new";
        }

        return "redirect:/access-denied";
    }

    @GetMapping("/delete")
    public String deleteTicket(@RequestParam("id") int id) {
        ticketsService.deleteById(id);
        return "redirect:/tickets/my";
    }

}
