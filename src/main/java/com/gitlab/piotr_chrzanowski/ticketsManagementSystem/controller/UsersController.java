package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.controller;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.dto.UserCredentialDTO;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.dto.UserInfoDTO;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.UsersService;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UsersController {

    private final int elementsPerPage = 15;
    private UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/list")
    public String listUsers(@RequestParam(name = "page", defaultValue = "1") Integer page,
                            @RequestParam(defaultValue = "id,asc") String sort,
                            Model model) {


        PageRequest pageRequest = Utility.parsePageRequest(page, sort, elementsPerPage);
        Page<User> users = usersService.findAll(pageRequest);
        model.addAttribute("users", users);
        model.addAttribute("pageNumbers", Utility.getListOfPages(users.getTotalPages()));

        return "user-list";
    }

    @GetMapping("/search")
    public String searchUserByName(@RequestParam(name = "name") String name,
                                   @RequestParam(name = "page", defaultValue = "1") Integer page,
                                   @RequestParam(defaultValue = "id,asc") String sort,
                                   Model model) {

        PageRequest pageRequest = Utility.parsePageRequest(page, sort, elementsPerPage);
        Page<User> users = usersService.findByName(name.trim(), pageRequest);
        model.addAttribute("users", users);
        model.addAttribute("pageNumbers", Utility.getListOfPages(users.getTotalPages()));

        return "user-list";
    }


    @GetMapping("/details")
    public String userDetails(@RequestParam("id") int id, Model model) {
        User user = usersService.findById(id);
        model.addAttribute("user", user);

        return "user-details";
    }

    @GetMapping("/createNew")
    public String createUser(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "user-new";
    }

    @PostMapping("/saveNew")
    public String saveUser(@Valid @ModelAttribute("user") User user, BindingResult bindingResult) {

        if (bindingResult.hasErrors()){
            return "user-new";
        }

        usersService.save(user);
        return "redirect:/users/list";
    }

    @GetMapping("/updateInfo")
    public String updateUserById(@RequestParam("id") int id, Model model) {
        User user = usersService.findById(id);

        if (usersService.validateUserPrivileges(user.getUsername()) == false) {
            return "redirect:/access-denied";
        }

        UserInfoDTO userInfoDTO = new UserInfoDTO();
        userInfoDTO.insertEntityData(user);

        model.addAttribute("user", userInfoDTO);
        return "user-profile";
    }

    @PostMapping("/saveInfo")
    public String saveUser(@Valid @ModelAttribute("user") UserInfoDTO userInfoDTO, BindingResult bindingResult) {

        User user = usersService.findById(userInfoDTO.getId());

        if (usersService.validateUserPrivileges(user.getUsername()) == false) {
            return "redirect:/access-denied";
        }

        if (bindingResult.hasErrors()){
            return "user-profile";
        }

        userInfoDTO.insertDtoData(user);
        usersService.save(user);
        return "redirect:/users/list";
    }

    @GetMapping("/updateCredential")
    public String updateUserCredential(@RequestParam("id") int id, Model model) {

        User user = usersService.findById(id);

        if (usersService.validateUserPrivileges(user.getUsername()) == false) {
            return "redirect:/access-denied";
        }

        UserCredentialDTO userCredentialDTO = new UserCredentialDTO();
        userCredentialDTO.insertEntityData(user);

        model.addAttribute("user", userCredentialDTO);

        return "user-credential";
    }

    @PostMapping("/saveCredential")
    public String saveUserCredential(@Valid @ModelAttribute("user") UserCredentialDTO user, BindingResult bindingResult) {

        if (usersService.validateUserPrivileges(user.getUsername()) == false) {
            return "redirect:/access-denied";
        }

        if (bindingResult.hasErrors()){
            return "user-credential";
        }

        User userObject = usersService.findById(user.getId());
        userObject.setPassword(user.getPassword());

        usersService.save(userObject);
        return "redirect:/users/list";
    }

    @GetMapping("/profile")
    public String updateUserByUsername(Model model) {
        User user = usersService.findByUsername(Utility.getLoggedUser().getUsername());
        model.addAttribute("user", user);
        return "user-profile";
    }

    @GetMapping("/delete")
    public String deleteUser(@RequestParam("id") int id) {
        usersService.deleteById(id);
        return "redirect:/users/list";
    }

}
