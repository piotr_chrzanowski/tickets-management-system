package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String showLoginPage() {
        return "login";
    }

    @GetMapping("/login/demo")
    public String showDemoLoginPage() {
        return "login-demo";
    }

    @GetMapping("/access-denied")
    public String accessDeniedPage() {
        return "denied";
    }

    @GetMapping("/")
    public String startupPage() {
        return  "redirect:/tickets/my";
    }


}
