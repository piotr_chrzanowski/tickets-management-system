package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.controller;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;


    @GetMapping("/statistics")
    public String tickedAddedPerMonth(Model model) {
        LinkedHashMap<String, Integer> ticketsPerMonth = statisticsService.calculateCreatedTicketNumberPerMonth();
        model.addAttribute("ticketsPerMonthY", ticketsPerMonth.keySet());
        model.addAttribute("ticketsPerMonthX", ticketsPerMonth.values());
        model.addAttribute("maxTicketNumber", Collections.max(ticketsPerMonth.values()));

        Map<String, Double>  ticketsPerStatus = statisticsService.calculateTicketNumberPerStatus();
        model.addAttribute("newTickets", ticketsPerStatus.get("New"));
        model.addAttribute("openedTickets", ticketsPerStatus.get("Open"));
        model.addAttribute("closedTickets", ticketsPerStatus.get("Closed"));

        return "statistics";
    }

}
