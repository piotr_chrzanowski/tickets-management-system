package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.dto;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class UserInfoDTO {
    private int id;
    @Email(message = "Wrong email format!")
    @NotEmpty(message = "Email empty!")
    private String email;
    @NotNull(message = "Roles empty!")
    private String roles;

    @NotEmpty(message = "First Name empty!")
    private String firstName;

    @NotEmpty(message = "Last Name empty!")
    private String lastName;

    @NotEmpty(message = "Department empty!")
    private String department;

    @NotEmpty(message = "Phone empty!")
    @Pattern(regexp = "(?<!\\w)(\\(?(\\+|00)?48\\)?)?[ -]?\\d{3}[ -]?\\d{3}[ -]?\\d{3}(?!\\w)", message = "Phone number not valid!")
    private String phone;

    public UserInfoDTO() {
    }

    public UserInfoDTO(int id, String firstName, String lastName, String department, String phone, String email, String roles) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.phone = phone;
        this.email = email;
        this.roles = roles;
    }

    public void insertEntityData(User user) {
        this.setId(user.getId());
        this.setRoles(user.getRoles());
        this.setEmail(user.getEmail());
        this.setDepartment(user.getDepartment());
        this.setFirstName(user.getFirstName());
        this.setLastName(user.getLastName());
        this.setPhone(user.getPhone());
    }

    public void insertDtoData(User user) {
        user.setId(this.getId());
        user.setEmail(this.getEmail());
        user.setDepartment(this.getDepartment());
        user.setFirstName(this.getFirstName());
        user.setLastName(this.getLastName());
        user.setPhone(this.getPhone());
        user.setRoles(this.getRoles());
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
