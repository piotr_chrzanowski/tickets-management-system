package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.dto;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.validators.UniqueUsername;

import javax.validation.constraints.NotEmpty;

public class UserCredentialDTO {

    private int id;
    @UniqueUsername
    private String username;

    @NotEmpty
    private String password;

    public UserCredentialDTO() {
    }

    public UserCredentialDTO(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public void insertEntityData(User user) {
        this.setId(user.getId());
        this.setPassword(user.getPassword());
        this.setUsername(user.getUsername());
    }

    public void insertDTOData(User user) {
        user.setId(this.getId());
        user.setPassword(this.getPassword());
        user.setUsername(this.getUsername());
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
