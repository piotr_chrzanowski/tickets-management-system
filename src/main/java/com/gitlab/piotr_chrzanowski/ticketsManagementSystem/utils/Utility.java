package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.utils;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.UserCredentials;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Utility {

    public static User getLoggedUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return ((UserCredentials) principal).getUser();
        }

        return null;
    }

    public static String getLoggedUserUserName() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return ((UserCredentials) principal).getUser().getUsername();
        }

        return null;
    }




    public static List<Integer> getListOfPages(int totalPages) {
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());

            return pageNumbers;
        } else {
            List<Integer> list = new ArrayList<>();
            list.add(1);
            return list;
        }
    }

    public static PageRequest parsePageRequest(Integer page, String sortOrder, Integer elementsPerPage) {
        PageRequest pageRequest = null;
        Sort.Order order = null;

        if (sortOrder.contains(",")) {
            String[] _sort = sortOrder.split(",");

            if (_sort[1].equals("asc")) {
                order = new Sort.Order(Sort.Direction.ASC, _sort[0]);
            } else {
                order = new Sort.Order(Sort.Direction.DESC, _sort[0]);
            }
        }

        if (page >= 1) {
            pageRequest = PageRequest.of(page - 1, elementsPerPage, Sort.by(order));
        } else {
            pageRequest = PageRequest.of(0, elementsPerPage, Sort.by(order));
        }

        return pageRequest;
    }

}
