package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.utils;

import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.util.LinkedList;

public class StatisticsUtility {

    public static class MonthAndYear {
        public int month;
        public int year;

        public MonthAndYear(int month, int year) {
            this.month = month;
            this.year = year;
        }
    }
    public static LinkedList<StatisticsUtility.MonthAndYear> getLastMonthAndYearList(int previousMonthsNumber){

        LinkedList<StatisticsUtility.MonthAndYear> results = new LinkedList<>();
        LocalDate date = LocalDate.now().minusMonths(previousMonthsNumber - 1);

        for (int i = 0; i < previousMonthsNumber; ++i) {
            results.add(new StatisticsUtility.MonthAndYear(date.getMonthValue(), date.getYear()));
            date = date.plusMonths(1);
        }

        return results;
    }

    public static String convertMonthToString(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }
}
