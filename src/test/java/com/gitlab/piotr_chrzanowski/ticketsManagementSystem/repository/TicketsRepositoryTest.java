package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Ticket;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@DataJpaTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TicketsRepositoryTest {
    @Autowired
    private TicketsRepository ticketsRepository;
    @Autowired
    private UsersRepository usersRepository;
    private Ticket ticket1;
    private Ticket ticket2;
    private Ticket ticket3;
    private User testUser1;
    private User testUser2;

    private LocalDateTime ticket1Date;
    private LocalDateTime ticket2Date;
    private LocalDateTime ticket3Date;


    @BeforeEach
    void setUp() {
        ticket1 = new Ticket(1, "Alpha", "Open", "High", "Software");
        ticket2 = new Ticket(2, "Aaaaa", "Open", "High", "Software");
        ticket3 = new Ticket(3, "Betaa", "Closed", "High", "Software");
        testUser1 = new User(1, "test1", "test1", true, "ROLE_USER,ROLE_ADMIN", "FirstNameUser1", "LastNameUser1", "TestDepartment1", "+48777777777", "test1@test.com");
        testUser2 = new User(2, "test2", "test2", true, "ROLE_USER,ROLE_ADMIN", "FirstNameUser2", "LastNameUser2", "TestDepartment", "+48777777777", "test2@test.com");

        ticket1Date = LocalDateTime.of(2022,11,11,11,11,11);
        ticket2Date = LocalDateTime.of(2022,11,01,06,44,33);
        ticket3Date = LocalDateTime.of(1995,05,02,23,55,55);

        ticket1.setCreated(ticket1Date);
        ticket2.setCreated(ticket2Date);
        ticket3.setCreated(ticket3Date);

        ticket1.setRequester(testUser1);
        ticket2.setRequester(testUser1);
        ticket3.setRequester(testUser2);

        testUser1.setTickets(Arrays.asList(ticket1, ticket2));
        testUser2.setTickets(Arrays.asList(ticket1, ticket3));

        usersRepository.save(testUser1);
        usersRepository.save(testUser2);

        ticketsRepository.save(ticket1);
        ticketsRepository.save(ticket2);
        ticketsRepository.save(ticket3);
    }


    @AfterEach
    void tearDown() {
        ticket1 = null;
        ticket2 = null;
        ticket3 = null;
        ticketsRepository.deleteAll();
    }


    @Test
    void findByTitleContaining_Full_Tittle() {
        //given
        String title = "Alpha";

        //when
        Page<Ticket> tickets = ticketsRepository.findByTitleContaining(title, PageRequest.of(0, 10));

        //then
        Assertions.assertEquals(tickets.getContent().get(0).getTitle(), ticket1.getTitle());
        Assertions.assertEquals(tickets.getContent().size(), 1);
    }

    @Test
    void findByTitleContaining_Part_Of_Tittle() {
        //given
        String title = "A";

        //when
        Page<Ticket> tickets = ticketsRepository.findByTitleContaining(title, PageRequest.of(0, 10));

        //then
        Assertions.assertEquals(tickets.getContent().get(0).getTitle(), ticket1.getTitle());
        Assertions.assertEquals(tickets.getContent().get(1).getTitle(), ticket2.getTitle());
        Assertions.assertEquals(2, tickets.getContent().size());
    }

    @Test
    void findByTitleContaining_Title_Not_Existing_In_Database() {
        //given
        String title = "Test";

        //when
        Page<Ticket> tickets = ticketsRepository.findByTitleContaining(title, PageRequest.of(0, 10));

        //then
        Assertions.assertTrue(tickets.getContent().isEmpty());
    }


    @Test
    void findByTitleContaining_Empty_Title_Returns_All_Results() {
        //given
        String title = "";

        //when
        Page<Ticket> tickets = ticketsRepository.findByTitleContaining(title, PageRequest.of(0, 10));

        //then
        Assertions.assertEquals(tickets.getContent().get(0).getTitle(), ticket1.getTitle());
        Assertions.assertEquals(tickets.getContent().get(1).getTitle(), ticket2.getTitle());
        Assertions.assertEquals(tickets.getContent().get(2).getTitle(), ticket3.getTitle());
        Assertions.assertEquals(3, tickets.getContent().size());
    }

    @Test
    void findByRequesterUsername_Returns_Correct_Tickets() {
        //given
        String username = testUser1.getUsername();

        //when
        Page<Ticket> tickets = ticketsRepository.findByRequesterUsername(username, PageRequest.of(0,10));

        //then
        Assertions.assertEquals(username, tickets.getContent().get(0).getRequester().getUsername());
        Assertions.assertEquals(username, tickets.getContent().get(1).getRequester().getUsername());
        Assertions.assertEquals(testUser1.getTickets().size(), tickets.getContent().size());
    }

    @Test
    void findByRequesterUsername_With_Wrong_Or_Empty_Username_Does_Not_Return_Tickets() {
        //given
        String emptyUsername = "";
        String wrongUsername = "userThatNotExist";

        //when
        Page<Ticket> emptyUsernameTickets = ticketsRepository.findByRequesterUsername(emptyUsername, PageRequest.of(0,10));
        Page<Ticket> wrongUsernameTickets = ticketsRepository.findByRequesterUsername(wrongUsername, PageRequest.of(0,10));

        //then
        Assertions.assertEquals(emptyUsernameTickets.getContent().size(), 0);
        Assertions.assertEquals(wrongUsernameTickets.getContent().size(), 0);
    }

    @Test
    void findByStatus_Returns_All_Tickets_With_That_Status()  {
        //given
        String newStatus = "New";
        int ticketsWithNewStatus = 0;

        String closedStatus = "Closed";
        int ticketsWithClosedStatus = 1;

        String openStatus = "Open";
        int ticketsWithOpenedStatus = 2;

        //when
        List<Ticket> newStatusTickets = ticketsRepository.findByStatus(newStatus);
        List<Ticket> closedStatusTickets = ticketsRepository.findByStatus(closedStatus);
        List<Ticket> openStatusTickets = ticketsRepository.findByStatus(openStatus);

        //then
        Assertions.assertEquals(newStatusTickets.size(), ticketsWithNewStatus);
        Assertions.assertEquals(closedStatusTickets.size(), ticketsWithClosedStatus);
        Assertions.assertEquals(openStatusTickets.size(), ticketsWithOpenedStatus);
    }

    @Test
    void findByStatus_Returns_Empty_List_When_Status_Is_Empty_Or_Wrong() {
        //given
        String emptyStatus = "";
        String wrongStatus = "Test";

        //when
        List<Ticket> emptyStatusTickets = ticketsRepository.findByStatus(emptyStatus);
        List<Ticket> wrongStatusTickets = ticketsRepository.findByStatus(wrongStatus);

        //then
        Assertions.assertEquals(0, emptyStatusTickets.size());
        Assertions.assertEquals(0, wrongStatusTickets.size());
    }

    @Test
    void findByMonthAndYearMatch_Returns_Number_Of_Ticket_Created_At_That_Date() {
        //given
        int ticket1Month = ticket1Date.getMonthValue();
        int ticket1Year = ticket1Date.getYear();

        int ticket3Month = ticket3Date.getMonthValue();
        int ticket3Year = ticket3Date.getYear();

        //when
        int numberOfTicketCreatedInNovember2022 = ticketsRepository.findByMonthAndYearMatch(ticket1Month, ticket1Year);
        int numberOfTicketCreatedInMay1995 = ticketsRepository.findByMonthAndYearMatch(ticket3Month, ticket3Year);

        //then
        Assertions.assertEquals(2, numberOfTicketCreatedInNovember2022);
        Assertions.assertEquals(1, numberOfTicketCreatedInMay1995);
    }

    @Test
    void findByMonthAndYearMatch_Returns_Zero_When_Date_Is_Incorrect() {
        //given
        int wrongMonth = 13;
        int wrongYear = 20005;

        //when
        int numberOfTicketWithWrongDate = ticketsRepository.findByMonthAndYearMatch(wrongMonth, wrongYear);

        //then
        Assertions.assertEquals(0, numberOfTicketWithWrongDate);

    }
}
