package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

@DataJpaTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UsersRepositoryTest {

    @Autowired
    private UsersRepository usersRepository;
    private User testUser1;
    private User testUser2;


    @BeforeEach
    void setUp() {
        testUser1 = new User(1, "test1", "test1", true, "ROLE_USER,ROLE_ADMIN", "FirstNameUser1", "LastNameUser1", "TestDepartment1", "+48777777777", "test1@test.com");
        testUser2 = new User(2, "test2", "test2", true, "ROLE_USER,ROLE_ADMIN", "FirstNameUser2", "LastNameUser2", "TestDepartment", "+48777777777", "test2@test.com");

        usersRepository.save(testUser1);
        usersRepository.save(testUser2);
    }


    @AfterEach
    void tearDown() {
        testUser1 = null;
        testUser2 = null;
        usersRepository.deleteAll();
    }

    @Test
    void findByFirstNameContainingOrLastNameContainingIgnoreCase_Found_User_By_Full_Name() {
        //given
        String firstName = testUser1.getFirstName();
        String lastName = testUser1.getLastName();

        //when
        Page<User> users = usersRepository.findByFirstNameContainingOrLastNameContainingIgnoreCase(firstName, lastName, PageRequest.of(0, 10));

        //then
        Assertions.assertEquals(firstName, users.getContent().get(0).getFirstName());
        Assertions.assertEquals(lastName, users.getContent().get(0).getLastName());
        Assertions.assertEquals(users.getContent().size(), 1);
    }

    @Test
    void findByFirstNameContainingOrLastNameContainingIgnoreCase_Found_User_By_First_Name_Only() {
        //given
        String firstName = testUser1.getFirstName();
        String lastName = testUser1.getLastName();

        //when
        Page<User> users = usersRepository.findByFirstNameContainingOrLastNameContainingIgnoreCase(firstName, firstName, PageRequest.of(0, 10));

        //then
        Assertions.assertEquals(firstName, users.getContent().get(0).getFirstName());
        Assertions.assertEquals(lastName, users.getContent().get(0).getLastName());
        Assertions.assertEquals(1, users.getContent().size());
    }
    @Test
    void findByFirstNameContainingOrLastNameContainingIgnoreCase_Found_User_By_Last_Name_Only() {
        //given
        String firstName = testUser1.getFirstName();
        String lastName = testUser1.getLastName();

        //when
        Page<User> users = usersRepository.findByFirstNameContainingOrLastNameContainingIgnoreCase(lastName, lastName, PageRequest.of(0, 10));

        //then
        Assertions.assertEquals(firstName, users.getContent().get(0).getFirstName());
        Assertions.assertEquals(lastName, users.getContent().get(0).getLastName());
        Assertions.assertEquals(users.getContent().size(), 1);
    }

    @Test
    void findByFirstNameContainingOrLastNameContainingIgnoreCase_Found_User_By_LetterContaining() {
        //given
        String firstNameU1 = testUser1.getFirstName();
        String lastNameU1 = testUser1.getLastName();
        String firstNameU2 = testUser2.getFirstName();
        String lastNameU2 = testUser2.getLastName();

        String firstLetter = testUser1.getFirstName().substring(0,1);

        //when
        Page<User> users = usersRepository.findByFirstNameContainingOrLastNameContainingIgnoreCase(firstLetter, firstLetter, PageRequest.of(0, 10));

        //then
        Assertions.assertEquals(firstNameU1, users.getContent().get(0).getFirstName());
        Assertions.assertEquals(lastNameU1, users.getContent().get(0).getLastName());
        Assertions.assertEquals(firstNameU2, users.getContent().get(1).getFirstName());
        Assertions.assertEquals(lastNameU2, users.getContent().get(1).getLastName());
        Assertions.assertEquals(users.getContent().size(), 2);
    }


    @Test
    void findByUsername_Found() {
        //given
        int id = testUser1.getId();
        String username = testUser1.getUsername();
        String password = testUser1.getPassword();
        String roles = testUser1.getRoles();

        //when
        Optional<User> user = usersRepository.findByUsername(username);

        //then
        Assertions.assertTrue(user.isPresent());
        Assertions.assertEquals(id, user.get().getId());
        Assertions.assertEquals(username, user.get().getUsername());
        Assertions.assertEquals(password, user.get().getPassword());
        Assertions.assertEquals(roles, user.get().getRoles());
    }

    @Test
    void findByUsername_NotFound() {
        //given
        String usernameNotExisting = "test3";

        //when
        Optional<User> user = usersRepository.findByUsername(usernameNotExisting);

        //then
        Assertions.assertFalse(user.isPresent());
    }


}
