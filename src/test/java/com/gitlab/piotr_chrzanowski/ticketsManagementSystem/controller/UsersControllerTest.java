package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.controller;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config.ApplicationConfigTest;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.dto.UserCredentialDTO;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.dto.UserInfoDTO;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.UserCredentials;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.UsersService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UsersController.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import(ApplicationConfigTest.class)
class UsersControllerTest {
    private MockMvc mockMvc;
    @MockBean
    private UsersService usersService;
    private User user1;
    private User user2;
    private Page<User> users;

    @Mock
    private Authentication authenticationMock;
    @Mock
    private SecurityContext securityContextMock;
    @Mock
    private UserCredentials userCredentialsMock;

    @Mock
    private User userMock;

    @Autowired
    public UsersControllerTest(MockMvc mockMvc, UsersService usersService) {
        this.mockMvc = mockMvc;
        this.usersService = usersService;
    }

    @BeforeEach
    void setUp() {
        user1 = new User(1, "test1", "test1", true, "ROLE_ADMIN", "FirstNameUser1", "LastNameUser1", "TestDepartment1", "+48777777777", "test1@test.com");
        user2 = new User(2, "test2", "test2", true, "ROLE_ADMIN", "FirstNameUser2", "LastNameUser2", "TestDepartment", "+48777777777", "test2@test.com");
        users = new PageImpl<>(Arrays.asList(user1, user2));

        authenticationMock = mock(Authentication.class);
        securityContextMock = mock(SecurityContext.class);
        userCredentialsMock = mock(UserCredentials.class);
        userMock = mock(User.class);
    }

    @Test
    @WithMockUser
    void listAllUsers_Return_Users_Attribute() throws Exception {
        //given
        given(usersService.findAll(any(Pageable.class))).willReturn(users);

        //when
        this.mockMvc.perform(get("/users/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("user-list"))
                .andExpect(model().attributeExists("pageNumbers"))
                .andExpect(model().attribute("pageNumbers", equalTo(Arrays.asList(new Integer[]{1}))))
                .andExpect(model().attributeExists("users"))
                .andExpect(model().attribute("users", users));

        //then
        verify(usersService, times(1)).findAll(any());
    }

    @Test
    @WithMockUser
    void searchUserByName_Should_Return_Users_Attribute() throws Exception {
        //given
        String nameToSearch = "test1";
        given(usersService.findByName(any(String.class), any(Pageable.class))).willReturn(users);

        //when
        this.mockMvc.perform(get("/users/search?name=" + nameToSearch))
                .andExpect(status().isOk())
                .andExpect(view().name("user-list"))
                .andExpect(model().attributeExists("pageNumbers"))
                .andExpect(model().attribute("pageNumbers", equalTo(Arrays.asList(new Integer[]{1}))))
                .andExpect(model().attributeExists("users"))
                .andExpect(model().attribute("users", users));

        //then
        verify(usersService, times(1)).findByName(eq(nameToSearch), any());
    }


    @Test
    @WithMockUser
    void userDetails_Should_Return_User_Attribute() throws Exception {
        //given
        int testId = 1;
        given(usersService.findById(testId)).willReturn(user1);

        //when
        this.mockMvc.perform(get("/users/details?id=1"))
                .andExpect(status().isOk())
                .andExpect(view().name("user-details"))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attribute("user", user1));

        //then
        verify(usersService, times(1)).findById(testId);
    }


    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteUser_Should_Redirect_To_User_List() throws Exception {
        //given
        int testId = 1;
        given(usersService.deleteById(testId)).willReturn(true);

        //when
        this.mockMvc.perform(get("/users/delete?id=1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/users/list"));

        //then
        verify(usersService, times(1)).deleteById(testId);
    }

    @Test
    @WithMockUser(roles = "USER")
    void deleteUser_Should_Forward_To_Access_Denied_When_Performed_By_User() throws Exception {
        //given
        int testId = 1;
        given(usersService.deleteById(testId)).willReturn(true);

        //when
        this.mockMvc.perform(get("/users/delete?id=1"))
                .andExpect(status().is4xxClientError())
                .andExpect(forwardedUrl("/access-denied"));

        //then
        verify(usersService, times(0)).deleteById(testId);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void createNew_Should_Add_Empty_User_Attribute() throws Exception {
        //given

        //when
        this.mockMvc.perform(get("/users/createNew"))
                .andExpect(status().isOk())
                .andExpect(view().name("user-new"))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attribute("user", hasProperty("id", equalTo(0))))
                .andExpect(model().attribute("user", hasProperty("firstName", equalTo(null))))
                .andExpect(model().attribute("user", hasProperty("lastName", equalTo(null))))
                .andExpect(model().attribute("user", hasProperty("phone", equalTo(null))))
                .andExpect(model().attribute("user", hasProperty("email", equalTo(null))));
    }

    @Test
    @WithMockUser(roles = "USER")
    void createNew_Forward_To_Access_Denied_When_Performed_By_User() throws Exception {
        //given

        //when
        this.mockMvc.perform(get("/users/createNew"))
                .andExpect(status().is4xxClientError())
                .andExpect(forwardedUrl("/access-denied"));

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void saveNew_Does_Not_Redirect_And_Save_When_Form_Has_Some_Errors() throws Exception {
        //given
        user1.setPhone("wrongPhoneNumber");

        //when
        this.mockMvc.perform(post("/users/saveNew")
                        .with(csrf())
                        .flashAttr("user", user1))
                .andExpect(view().name("user-new"))
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attributeHasFieldErrorCode("user", "phone", "Pattern"));

        //then
        verify(usersService, times(0)).save(user1);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void saveNew_Redirect_To_User_List_And_Perform_Saving() throws Exception {
        //given

        //when
        this.mockMvc.perform(post("/users/saveNew")
                        .with(csrf())
                        .flashAttr("user", user1))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/users/list"));

        //then
        verify(usersService, times(1)).save(user1);
    }

    @Test
    @WithMockUser(roles = "USER")
    void saveNew_Forward_To_Access_Denied_When_Performed_By_User() throws Exception {
        //given

        //when
        this.mockMvc.perform(post("/users/saveNew")
                        .with(csrf())
                        .flashAttr("user", user1))
                .andExpect(status().is4xxClientError())
                .andExpect(forwardedUrl("/access-denied"));

        //then
        verify(usersService, times(0)).save(user1);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateInfo_Should_Add_UserInfoDTO_Attribute() throws Exception {
        //given
        int testId = 1;
        given(usersService.findById(testId)).willReturn(user1);
        given(usersService.validateUserPrivileges(any())).willReturn(true);

        UserInfoDTO userInfoDTO = new UserInfoDTO();
        userInfoDTO.insertEntityData(user1);

        //when
        this.mockMvc.perform(get("/users/updateInfo?id=1"))
                .andExpect(status().isOk())
                .andExpect(view().name("user-profile"))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attribute("user", hasProperty("id", equalTo(userInfoDTO.getId()))))
                .andExpect(model().attribute("user", hasProperty("firstName", equalTo(userInfoDTO.getFirstName()))))
                .andExpect(model().attribute("user", hasProperty("lastName", equalTo(userInfoDTO.getLastName()))))
                .andExpect(model().attribute("user", hasProperty("phone", equalTo(userInfoDTO.getPhone()))))
                .andExpect(model().attribute("user", hasProperty("department", equalTo(userInfoDTO.getDepartment()))))
                .andExpect(model().attribute("user", hasProperty("roles", equalTo(userInfoDTO.getRoles()))))
                .andExpect(model().attribute("user", hasProperty("email", equalTo(userInfoDTO.getEmail()))));

        //then
        verify(usersService, times(1)).findById(testId);
    }

    @Test
    @WithMockUser(roles = "USER")
    void updateInfo_Forward_To_Access_Denied_When_Performed_By_User() throws Exception {
        //given
        int testId = 1;
        given(usersService.findById(testId)).willReturn(user1);

        //when
        this.mockMvc.perform(get("/users/updateInfo?id=1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/access-denied"));

        //then
        verify(usersService, times(1)).findById(testId);
    }


    @Test
    @WithMockUser(roles = "USER")
    void saveInfo_Forward_To_Access_Denied_When_Performed_By_User_Without_Privileges() throws Exception {
        //given
        int testId = 1;
        given(usersService.findById(testId)).willReturn(user1);
        given(usersService.validateUserPrivileges(any())).willReturn(false);

        UserInfoDTO userInfoDTO = new UserInfoDTO();
        userInfoDTO.insertEntityData(user1);

        //when
        this.mockMvc.perform(post("/users/saveInfo")
                .with(csrf())
                .flashAttr("user", userInfoDTO))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/access-denied"));

        //then
        verify(usersService, times(1)).findById(testId);
    }

    @Test
    @WithMockUser(roles = "USER")
    void saveInfo_Should_Save_User_Info_When_User_Have_Privileges() throws Exception {
        //given
        int testId = 1;
        given(usersService.findById(testId)).willReturn(user1);
        given(usersService.validateUserPrivileges(any())).willReturn(true);

        UserInfoDTO userInfoDTO = new UserInfoDTO();
        userInfoDTO.insertEntityData(user1);

        //when
        this.mockMvc.perform(post("/users/saveInfo")
                        .with(csrf())
                        .flashAttr("user", userInfoDTO))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/users/list"));

        //then
        verify(usersService, times(1)).findById(testId);
        verify(usersService, times(1)).save(any());
    }

    @Test
    @WithMockUser(roles = "USER")
    void saveInfo_Should_Not_Save_User_Info_When_Form_Has_Errors() throws Exception {
        //given
        int testId = 1;
        given(usersService.findById(testId)).willReturn(user1);
        given(usersService.validateUserPrivileges(any())).willReturn(true);

        UserInfoDTO userInfoDTO = new UserInfoDTO();
        userInfoDTO.insertEntityData(user1);
        userInfoDTO.setPhone("wrongPhoneNumber");

        //when
        this.mockMvc.perform(post("/users/saveInfo")
                        .with(csrf())
                        .flashAttr("user", userInfoDTO))
                .andExpect(view().name("user-profile"))
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attributeHasFieldErrorCode("user", "phone", "Pattern"));
        //then
        verify(usersService, times(1)).findById(testId);
        verify(usersService, times(0)).save(any());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateCredential_Should_Add_UserCredentialDTO_Attribute() throws Exception {
        //given
        int testId = 1;
        given(usersService.findById(testId)).willReturn(user1);
        given(usersService.validateUserPrivileges(any())).willReturn(true);

        UserCredentialDTO userCredentialDTO = new UserCredentialDTO();
        userCredentialDTO.insertEntityData(user1);

        //when
        this.mockMvc.perform(get("/users/updateCredential?id=1"))
                .andExpect(status().isOk())
                .andExpect(view().name("user-credential"))
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attribute("user", hasProperty("id", equalTo(userCredentialDTO.getId()))))
                .andExpect(model().attribute("user", hasProperty("username", equalTo(userCredentialDTO.getUsername()))))
                .andExpect(model().attribute("user", hasProperty("password", equalTo(userCredentialDTO.getPassword()))));

        //then
        verify(usersService, times(1)).findById(testId);
    }

    @Test
    @WithMockUser(roles = "USER")
    void updateCredential_Forward_To_Access_Denied_When_Performed_By_User_Without_Privileges() throws Exception {
        //given
        int testId = 1;
        given(usersService.findById(testId)).willReturn(user1);
        given(usersService.validateUserPrivileges(any())).willReturn(false);

        //when
        this.mockMvc.perform(get("/users/updateCredential?id=1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/access-denied"));

        //then
        verify(usersService, times(1)).findById(testId);
    }



    @Test
    @WithMockUser(roles = "USER")
    void saveCredential_Forward_To_Access_Denied_When_Performed_By_User_Without_Privileges() throws Exception {
        //given
        int testId = 1;
        given(usersService.findById(testId)).willReturn(user1);
        given(usersService.validateUserPrivileges(any())).willReturn(false);

        UserCredentialDTO userCredentialDTO = new UserCredentialDTO();
        userCredentialDTO.insertEntityData(user1);

        //when
        this.mockMvc.perform(post("/users/saveCredential")
                        .with(csrf())
                        .flashAttr("user", userCredentialDTO))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/access-denied"));

        //then
        verify(usersService, times(0)).findById(testId);
    }

    @Test
    @WithMockUser(roles = "USER")
    void saveCredential_Should_Save_User_Credential_When_User_Have_Privileges() throws Exception {
        //given
        int testId = 1;
        given(usersService.findById(testId)).willReturn(user1);
        given(usersService.validateUserPrivileges(any())).willReturn(true);

        UserCredentialDTO userCredentialDTO = new UserCredentialDTO();
        userCredentialDTO.insertEntityData(user1);

        //when
        this.mockMvc.perform(post("/users/saveCredential")
                        .with(csrf())
                        .flashAttr("user", userCredentialDTO))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/users/list"));

        //then
        verify(usersService, times(1)).findById(testId);
        verify(usersService, times(1)).save(any());
    }

    @Test
    @WithMockUser(roles = "USER")
    void saveCredential_Should_Not_Save_User_Credential_When_Form_Has_Errors() throws Exception {
        //given
        int testId = 1;
        given(usersService.findById(testId)).willReturn(user1);
        given(usersService.validateUserPrivileges(any())).willReturn(true);

        UserCredentialDTO userCredentialDTO = new UserCredentialDTO();
        userCredentialDTO.insertEntityData(user1);
        userCredentialDTO.setPassword("");

        //when
        this.mockMvc.perform(post("/users/saveCredential")
                        .with(csrf())
                        .flashAttr("user", userCredentialDTO))
                .andExpect(view().name("user-credential"))
                .andExpect(model().attributeExists("user"))
                .andExpect(model().attributeHasFieldErrorCode("user", "password", "NotEmpty"));
        //then
        verify(usersService, times(0)).findById(testId);
        verify(usersService, times(0)).save(any());
    }













//    @Test
//    @WithMockUser(roles = {"ADMIN"})
//    void showUserProfile_Will_Return_Logged_User_Profile() throws Exception {
//        //given
//        SecurityContextHolder.setContext(securityContextMock);
//        given(securityContextMock.getAuthentication()).willReturn(authenticationMock);
//        given(securityContextMock.getAuthentication().getPrincipal()).willReturn(userCredentialsMock);
//        given(userCredentialsMock.getUser()).willReturn(userMock);
//
//        //when
//        this.mockMvc.perform(get("/users/profile"))
//                .andExpect(status().isOk())
//                .andExpect(view().name("user-new"))
//                .andExpect(content().contentType("text/html;charset=UTF-8"))
//                .andExpect(model().attributeExists("user"))
//                .andExpect(model().attribute("user", equalTo(user1)));
//
//        //then
//        verify(usersService, times(1)).findByUsername(any());
//    }


}

