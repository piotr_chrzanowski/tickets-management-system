package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.controller;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config.ApplicationConfigTest;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Ticket;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.TicketDetails;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.TicketsService;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.UsersService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TicketsController.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import(ApplicationConfigTest.class)
class TicketsControllerTest {
    private MockMvc mockMvc;
    @MockBean
    private TicketsService ticketsService;
    @MockBean
    private UsersService usersService;

    private Ticket ticket1;
    private Ticket ticket2;
    private Ticket ticket3;
    private TicketDetails ticketDetails1;
    private TicketDetails ticketDetails2;
    private TicketDetails ticketDetails3;
    private User requester;
    private Page<Ticket> tickets;



    @Autowired
    public TicketsControllerTest(MockMvc mockMvc, TicketsService ticketsService, UsersService usersService) {
        this.mockMvc = mockMvc;
        this.ticketsService = ticketsService;
        this.usersService = usersService;
    }

    @BeforeEach
    void setUp() {
        requester  = new User(1, "test", "test", true, "ROLE_ADMIN", "test", "test", "test", "+48777777777", "test1@test.com");
        ticket1 = new Ticket(1, "Alpha", "Open", "High", "Software");
        ticket2 = new Ticket(2, "A", "Open", "High", "Software");
        ticket3 = new Ticket(3, "Beta", "Open", "High", "Software");
        ticketDetails1 = new TicketDetails(1, "details for ticket 1");
        ticketDetails2 = new TicketDetails(2, "details for ticket 2");
        ticketDetails3 = new TicketDetails(2, "details for ticket 3");

        ticket1.setTicketDetails(ticketDetails1);
        ticket2.setTicketDetails(ticketDetails2);
        ticket3.setTicketDetails(ticketDetails3);

        ticketDetails1.setTicket(ticket1);
        ticketDetails2.setTicket(ticket2);
        ticketDetails3.setTicket(ticket3);

        ticket1.setRequester(requester);
        ticket2.setRequester(requester);
        ticket3.setRequester(requester);

        tickets = new PageImpl<>(Arrays.asList(ticket1, ticket2, ticket3));
        requester.setTickets(tickets.toList());
    }



    @Test
    @WithMockUser(roles = "ADMIN")
    void listAllTicket_Return_Tickets_Attribute() throws Exception {
        //given
        given(ticketsService.findAll(any(Pageable.class))).willReturn(tickets);

        //when
        this.mockMvc.perform(get("/tickets/all"))
            .andExpect(status().isOk())
            .andExpect(view().name("ticket-list"))
            .andExpect(model().attributeExists("pageNumbers"))
            .andExpect(model().attribute("pageNumbers", equalTo(Arrays.asList(new Integer[]{1}))))
            .andExpect(model().attributeExists("tickets"))
            .andExpect(model().attribute("tickets", tickets));

        //then
        verify(ticketsService, times(1)).findAll(any());
    }

    @Test
    @WithMockUser(roles = "USER")
    void listAllTicket_Forward_To_Access_Denied_When_Performed_By_User() throws Exception {
        //given
        given(ticketsService.findAll(any(Pageable.class))).willReturn(tickets);

        //when
        this.mockMvc.perform(get("/tickets/all"))
                .andExpect(status().is4xxClientError())
                .andExpect(forwardedUrl("/access-denied"));

        //then
        verify(ticketsService, times(0)).findAll(any());
    }

//    @Test
//    @WithMockUser(roles = "USER")
//    void listMyTicket_Return_Tickets_Attribute() throws Exception {
//        //given
//        given(ticketsService.findAll(any(Pageable.class))).willReturn(tickets);
//
//        //when
//        this.mockMvc.perform(get("/tickets/my"))
//                .andExpect(status().isOk())
//                .andExpect(view().name("ticket-list"))
//                .andExpect(model().attributeExists("pageNumbers"))
//                .andExpect(model().attribute("pageNumbers", equalTo(Arrays.asList(new Integer[]{1}))))
//                .andExpect(model().attributeExists("tickets"))
//                .andExpect(model().attribute("tickets", tickets));
//
//        //then
//        verify(ticketsService, times(1)).findAllForRequester(any(), any());
//    }

    @Test
    @WithMockUser
    void searchTicketByTitle_Return_Tickets_Attribute() throws Exception {
        //given
        Page<Ticket> ticketPage = new PageImpl<>(Arrays.asList(ticket1, ticket2));
        String titleToSearch = "Alpha";
        given(ticketsService.findByTitleContaining(any(String.class), any(Pageable.class))).willReturn(ticketPage);

        //when
        this.mockMvc.perform(get("/tickets/search?title=" + titleToSearch))
                .andExpect(status().isOk())
                .andExpect(view().name("ticket-list"))
                .andExpect(model().attributeExists("pageNumbers"))
                .andExpect(model().attribute("pageNumbers", equalTo(Arrays.asList(new Integer[]{1}))))
                .andExpect(model().attributeExists("tickets"))
                .andExpect(model().attribute("tickets", ticketPage));

        //then
        verify(ticketsService, times(1)).findByTitleContaining(any(), any(Pageable.class));
    }

    @Test
    @WithMockUser
    void saveTicket_Should_Save_Ticket() throws Exception {
        //given

        //when
        this.mockMvc.perform(post("/tickets/save")
                .with(csrf())
                .flashAttr("ticket", ticket1))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/tickets/my"));

        //then
        verify(ticketsService, times(1)).save(ticket1);
    }
    @Test
    @WithMockUser
    void saveTicket_Should_Not_Save_Ticket_When_Form_Contain_Some_Errors() throws Exception {
        //given
        ticket1.setTitle("");

        //when
        this.mockMvc.perform(post("/tickets/save")
                        .with(csrf())
                        .flashAttr("ticket", ticket1))
                .andExpect(view().name("ticket-new"))
                .andExpect(model().attributeExists("ticket"));

        //then
        verify(ticketsService, times(0)).save(ticket1);
    }

    @Test
    @WithMockUser
    void createTicket_Should_Return_Empty_Ticket_Attribute() throws Exception {
        //given

        //when
        this.mockMvc.perform(get("/tickets/create"))
            .andExpect(status().isOk())
            .andExpect(view().name("ticket-new"))
            .andExpect(model().attributeExists("ticket"))
            .andExpect(model().attribute("ticket", hasProperty("id", equalTo(0))))
            .andExpect(model().attribute("ticket", hasProperty("title", equalTo(null))));

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateTicket_Should_Return_Ticket_Form_When_Validation_Pass() throws Exception {
        //given
        int testId = 1;
        given(ticketsService.findById(testId)).willReturn(ticket1);
        given(ticketsService.validateAuthorPrivileges(any())).willReturn(true);


        //when
        this.mockMvc.perform(get("/tickets/update?id=1"))
            .andExpect(status().isOk())
            .andExpect(view().name("ticket-new"))
            .andExpect(model().attributeExists("ticket"))
            .andExpect(model().attribute("ticket", equalTo(ticket1)));

        //then
        verify(ticketsService, times(1)).findById(testId);
    }

    @Test
    @WithMockUser
    void updateTicket_Redirect_To_Access_Denied_When_Validation_Failed() throws Exception {
        //given
        int testId = 1;
        given(ticketsService.findById(testId)).willReturn(ticket1);
        given(ticketsService.validateAuthorPrivileges(any())).willReturn(false);

        //when
        this.mockMvc.perform(get("/tickets/update?id=1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/access-denied"));

        //then
        verify(ticketsService, times(1)).findById(testId);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteTicket_Should_Delete_Ticket_And_Redirect() throws Exception {
        //given
        given(ticketsService.deleteById(1)).willReturn(true);

        //when
        this.mockMvc.perform(get("/tickets/delete?id=1"))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("/tickets/my"));

        //then
        verify(ticketsService, times(1)).deleteById(1);
    }

    @Test
    @WithMockUser(roles = "USER")
    void deleteTicket_Should_Forward_To_Access_Denied_When_Performed_By_User() throws Exception {
        //given
        given(ticketsService.deleteById(1)).willReturn(true);

        //when
        this.mockMvc.perform(get("/tickets/delete?id=1"))
                .andExpect(status().is4xxClientError())
                .andExpect(forwardedUrl("/access-denied"));

        //then
        verify(ticketsService, times(0)).deleteById(1);
    }

//    @Test
//    @WithMockUser(roles = "ADMIN")
//    void getTicketDetails_Should_Show_Ticket_Details_When_Performed_By_Admin() throws Exception {
//        //given
//        given(ticketsService.findById(1)).willReturn(ticket1);
//        given(ticketsService.validateAuthorPrivileges(any())).willReturn(true);
//
//        //when
//        this.mockMvc.perform(get("/tickets/details?id=1"))
//            .andExpect(status().isOk())
//            .andExpect(view().name("ticket-details"))
//            .andExpect(model().attributeExists("ticket"))
//            .andExpect(model().attribute("ticket", ticket1));
//
//        //then
//        verify(ticketsService, times(1)).findById(1);
//    }

    @Test
    @WithMockUser(roles = "USER")
    void getTicketDetails_Should_Redirect_To_Access_Denied_When_Performed_By_User() throws Exception {
        //given
        given(ticketsService.findById(1)).willReturn(ticket1);
        given(ticketsService.validateAuthorPrivileges(any())).willReturn(false);

        //when
        this.mockMvc.perform(get("/tickets/details?id=1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/access-denied"));

        //then
        verify(ticketsService, times(1)).findById(1);
    }
}