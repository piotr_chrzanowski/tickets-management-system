package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.controller;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config.ApplicationConfigTest;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.StatisticsService;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.UsersService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(StatisticsController.class)
@ActiveProfiles("test")
@Import(ApplicationConfigTest.class)
public class StatisticsControllerTest {
    private MockMvc mockMvc;
    @MockBean
    private StatisticsService statisticsService;
    @MockBean
    private UsersService usersService;

    @Autowired
    public StatisticsControllerTest(MockMvc mockMvc, StatisticsService statisticsService, UsersService usersService) {
        this.mockMvc = mockMvc;
        this.statisticsService = statisticsService;
        this.usersService = usersService;
    }

    private void initializeTicketPerMonthMap(LinkedHashMap<String, Integer> map) {
        map.put("January", 1);
        map.put("December", 1);
    }

    private void initializeTicketPerStatusMap(Map<String, Double> map) {
        map.put("New", 0.1);
        map.put("Open", 0.4);
        map.put("Closed", 0.5);
    }



    @Test
    @WithMockUser(roles = "ADMIN")
    void statistics_Should_Add_Charts_Attributes() throws Exception {
        //given
        LinkedHashMap<String, Integer> testTicketMonthMap = new LinkedHashMap<>();
        Map<String, Double> testTicketStautsMap = new HashMap<>();

        initializeTicketPerMonthMap(testTicketMonthMap);
        initializeTicketPerStatusMap(testTicketStautsMap);

        given(statisticsService.calculateCreatedTicketNumberPerMonth()).willReturn(testTicketMonthMap);
        given(statisticsService.calculateTicketNumberPerStatus()).willReturn(testTicketStautsMap);


        //when
        this.mockMvc.perform(get("/statistics"))
                .andExpect(status().isOk())
                .andExpect(view().name("statistics"))
                .andExpect(model().attributeExists("ticketsPerMonthY"))
                .andExpect(model().attributeExists("ticketsPerMonthX"))
                .andExpect(model().attributeExists("maxTicketNumber"))

                .andExpect(model().attributeExists("newTickets"))
                .andExpect(model().attributeExists("openedTickets"))
                .andExpect(model().attributeExists("closedTickets"));


        //then
        verify(statisticsService, times(1)).calculateCreatedTicketNumberPerMonth();
        verify(statisticsService, times(1)).calculateCreatedTicketNumberPerMonth();

    }
}
