package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.controller;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config.ApplicationConfigTest;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.TicketsService;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service.UsersService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(LoginController.class)
@ActiveProfiles("test")
@Import(ApplicationConfigTest.class)
public class LoginControllerTest {
    private MockMvc mockMvc;

    @MockBean
    private TicketsService ticketsService;
    @MockBean
    private UsersService usersService;
    @Autowired
    public LoginControllerTest(MockMvc mockMvc, TicketsService ticketsService, UsersService usersService) {
        this.mockMvc = mockMvc;
        this.ticketsService = ticketsService;
        this.usersService = usersService;
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void login_Should_Forward_To_Login_Page() throws Exception {
        //given

        //when
        this.mockMvc.perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("login"));

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void login_demo_Should_Forward_To_Login_Demo_Page() throws Exception {
        //given

        //when
        this.mockMvc.perform(get("/login/demo"))
                .andExpect(status().isOk())
                .andExpect(view().name("login-demo"));

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void access_denied_Should_Forward_To_Denied_Page() throws Exception {
        //given

        //when
        this.mockMvc.perform(get("/access-denied"))
                .andExpect(status().isOk())
                .andExpect(view().name("denied"));

    }
}
