package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config.ApplicationConfigTest;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.Ticket;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository.TicketsRepository;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository.UsersRepository;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.utils.StatisticsUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import(ApplicationConfigTest.class)
class StatisticsServiceImplTest {
    @Mock
    private TicketsRepository ticketsRepository;
    @Mock
    private UsersRepository usersRepository;
    private StatisticsService statisticsService;

    @Mock
    List<Ticket> newTicketList;
    @Mock
    List<Ticket> openTicketList;
    @Mock
    List<Ticket> closedTicketList;

    @BeforeEach
    void setUp() {
        ticketsRepository = mock(TicketsRepository.class);
        usersRepository = mock(UsersRepository.class);
        newTicketList = mock(List.class);
        openTicketList = mock(List.class);
        closedTicketList = mock(List.class);

        statisticsService = new StatisticsServiceImpl(ticketsRepository, usersRepository);
    }

    @Test
    void calculateTicketNumberPerStatus_Returns_Percentage_Map_Of_Different_Status() {
        //given
        int newTicketCount = 10;
        int openTicketCount = 5;
        int closedTicketCount = 0;
        double sum = newTicketCount + openTicketCount + closedTicketCount;

        given(ticketsRepository.findByStatus("New")).willReturn(newTicketList);
        given(ticketsRepository.findByStatus("Open")).willReturn(openTicketList);
        given(ticketsRepository.findByStatus("Closed")).willReturn(closedTicketList);
        given(newTicketList.size()).willReturn(newTicketCount);
        given(openTicketList.size()).willReturn(openTicketCount);
        given(closedTicketList.size()).willReturn(closedTicketCount);

        //when
        Map<String, Double> results = statisticsService.calculateTicketNumberPerStatus();

        //then
        Assertions.assertEquals((newTicketCount/sum * 100.00), results.get("New"));
        Assertions.assertEquals((openTicketCount/sum * 100.00), results.get("Open"));
        Assertions.assertEquals((closedTicketCount/sum * 100.00), results.get("Closed"));

        verify(ticketsRepository, times(1)).findByStatus("New");
        verify(ticketsRepository, times(1)).findByStatus("Open");
        verify(ticketsRepository, times(1)).findByStatus("Closed");
    }

    @Test
    void calculateTicketNumberPerMonth_Returns_Map_Of_Month_And_Ticket_Number() {
        //given
        List<Integer> ticketsPerMonth = Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12);
        List<StatisticsUtility.MonthAndYear> last12Months = StatisticsUtility.getLastMonthAndYearList(12);

        for(int i = 0; i < 12; ++i) {
            given(ticketsRepository.findByMonthAndYearMatch(last12Months.get(i).month,last12Months.get(i).year)).willReturn(ticketsPerMonth.get(i));
        }

        //when
        LinkedHashMap<String, Integer> results = statisticsService.calculateCreatedTicketNumberPerMonth();

        //then
        for (int i = 0; i < 12; ++i) {
            Assertions.assertEquals(ticketsPerMonth.get(i), results.get(StatisticsUtility.convertMonthToString(i + 1)));
        }

        verify(ticketsRepository, times(12)).findByMonthAndYearMatch(anyInt(),anyInt());
    }

}