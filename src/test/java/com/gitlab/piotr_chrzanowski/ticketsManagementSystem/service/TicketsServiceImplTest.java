package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config.ApplicationConfigTest;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.*;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository.CommentsRepository;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository.TicketsRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import(ApplicationConfigTest.class)
class TicketsServiceImplTest {
    @Mock
    private TicketsRepository ticketsRepository;
    @Mock
    private CommentsRepository commentsRepository;
    @Mock
    private TicketDetails ticketDetails1;
    @Mock
    private Ticket ticket1;

    @Mock
    private Comment comment1;
    @Mock
    private Page<Ticket> ticketsPage;
    @Mock
    private Pageable pageable;
    private TicketsService ticketsService;
    private AutoCloseable autoCloseable;
    @Mock
    private Authentication authentication;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private User user1;
    @Mock
    private UserCredentials userCredentials;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        ticketsRepository = mock(TicketsRepository.class);
        commentsRepository = mock(CommentsRepository.class);

        ticket1 = mock(Ticket.class);
        ticketDetails1 = mock(TicketDetails.class);
        comment1 = mock(Comment.class);
        ticketsPage = mock(Page.class);
        pageable = mock(Pageable.class);

        ticketsService = new TicketsServiceImpl(ticketsRepository, commentsRepository);

        authentication = mock(Authentication.class);
        securityContext = mock(SecurityContext.class);
        user1 = mock(User.class);
        userCredentials = mock(UserCredentials.class);
        SecurityContextHolder.setContext(securityContext);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }
        @Test
    void findAll_Return_Page_Of_Tickets() {
        //given
        given(ticketsRepository.findAll(any(Pageable.class))).willReturn(ticketsPage);

        //when
        Assertions.assertEquals(ticketsPage, ticketsService.findAll(pageable));

        //then
        verify(ticketsRepository, times(1)).findAll(any(Pageable.class));
    }

    @Test
    void findAllTitleContaining_Return_Page_Of_Tickets() {
        //given
        given(ticketsRepository.findByTitleContaining(any(String.class), any(Pageable.class))).willReturn(ticketsPage);

        //when
        Assertions.assertEquals(ticketsPage, ticketsService.findByTitleContaining("", pageable));

        //then
        verify(ticketsRepository, times(1)).findByTitleContaining(any(String.class), any(Pageable.class));
    }

    @Test
    void findAllForRequester_Returns_Requester_Tickets() {
        //given
        given(ticketsRepository.findByRequesterUsername(any(String.class), any(Pageable.class))).willReturn(ticketsPage);

        //when
        Assertions.assertEquals(ticketsPage, ticketsService.findAllForRequester("", pageable));

        //then
        verify(ticketsRepository, times(1)).findByRequesterUsername(any(String.class), any(Pageable.class));
    }

    @Test
    void findById_Return_Ticket_With_Id_1() {
        //given
        int testId = 1;
        given(ticketsRepository.findById(testId)).willReturn(Optional.of(ticket1));

        //when
        Assertions.assertEquals(ticketsService.findById(testId), ticket1);

        //then
        verify(ticketsRepository, times(1)).findById(testId);
    }

    @Test
    void findById_Return_Null_When_Object_Not_Exist() { // should throw
        //given
        int testId = 3;
        given(ticketsRepository.findById(testId)).willReturn(Optional.<Ticket>empty());

        //when
        Assertions.assertEquals(ticketsService.findById(testId), null);

        //then
        verify(ticketsRepository, times(1)).findById(testId);
    }

    @Test
    void save_Should_Return_Saved_Ticket() {
        //given
        given(ticketsRepository.save(ticket1)).willReturn(ticket1);
        given(ticket1.getTicketDetails()).willReturn(ticketDetails1);
        given(ticket1.getRequester()).willReturn(new User());

        //when
        Assertions.assertEquals(ticketsService.save(ticket1), ticket1);

        //then
        verify(ticketsRepository, times(1)).save(ticket1);
    }

    @Test
    void save_Should_Assign_TicketDetails_To_Ticket() {
        //given
        given(ticketsRepository.save(ticket1)).willReturn(ticket1);
        given(ticket1.getTicketDetails()).willReturn(ticketDetails1);
        given(ticket1.getRequester()).willReturn(new User());

        //when
        Assertions.assertEquals(ticketsService.save(ticket1), ticket1);

        //then
        verify(ticketDetails1, times(1)).setTicket(ticket1);
    }

    @Test
    void save_Should_Assign_Requester_To_Ticket() {
        //given
        given(ticketsRepository.save(ticket1)).willReturn(ticket1);
        given(ticket1.getTicketDetails()).willReturn(ticketDetails1);
        given(ticket1.getRequester()).willReturn(null);
        given(securityContext.getAuthentication()).willReturn(authentication);

        //when
        Assertions.assertEquals(ticketsService.save(ticket1), ticket1);

        //then
        verify(ticket1, times(1)).setRequester(any());
    }

    @Test
    void save_Should_Set_Up_Last_Updated_Date() {
        //given
        given(ticketsRepository.save(ticket1)).willReturn(ticket1);
        given(ticket1.getTicketDetails()).willReturn(ticketDetails1);
        given(ticket1.getRequester()).willReturn(new User());

        //when
        Assertions.assertEquals(ticketsService.save(ticket1), ticket1);

        //then
        verify(ticket1, times(1)).setLastUpdated(any());
    }

    @Test
    void save_Should_Add_Creation_Date_When_Ticket_Is_New() {
        //given
        given(ticketsRepository.save(ticket1)).willReturn(ticket1);
        given(ticket1.getTicketDetails()).willReturn(ticketDetails1);
        given(ticket1.getCreated()).willReturn(null);
        given(ticket1.getRequester()).willReturn(new User());

        //when
        Assertions.assertEquals(ticketsService.save(ticket1), ticket1);

        //then
        verify(ticket1, times(1)).setCreated(any());
    }

    @Test
    void save_Should_NOT_Add_Creation_Date_When_Ticket_Is_NOT_New() {
        //given
        given(ticketsRepository.save(ticket1)).willReturn(ticket1);
        given(ticket1.getTicketDetails()).willReturn(ticketDetails1);
        given(ticket1.getCreated()).willReturn(LocalDateTime.now());
        given(ticket1.getRequester()).willReturn(new User());

        //when
        Assertions.assertEquals(ticketsService.save(ticket1), ticket1);

        //then
        verify(ticket1, times(0)).setCreated(any());
    }

    @Test
    void save_Should_NOT_Set_Requester_If_It_Has_Been_Already_Set() {
        //given
        given(ticketsRepository.save(ticket1)).willReturn(ticket1);
        given(ticket1.getTicketDetails()).willReturn(ticketDetails1);
        given(ticket1.getRequester()).willReturn(new User());

        //when
        Assertions.assertEquals(ticketsService.save(ticket1), ticket1);

        //then
        verify(ticket1, times(0)).setRequester(any());
    }


    @Test
    void deleteAll_Delete_All_Tickets() {
        //given

        //when
        Assertions.assertTrue(ticketsService.deleteAll());

        //then
        verify(ticketsRepository, times(1)).deleteAll();
    }

    @Test
    void deleteById_Delete_Ticket_With_Id_1() {
        //given
        int testId = 1;

        //when
        Assertions.assertTrue(ticketsService.deleteById(testId));

        //then
        verify(ticketsRepository, times(1)).deleteById(testId);
    }

    @Test
    void saveComment_Should_Assign_Author_To_Comment() {
        //given
        given(commentsRepository.save(comment1)).willReturn(comment1);
        given(securityContext.getAuthentication()).willReturn(authentication);
        given(securityContext.getAuthentication().getPrincipal()).willReturn(userCredentials);
        given(userCredentials.getUser()).willReturn(user1);

        //when
        Comment comment = ticketsService.saveComment(comment1);

        //then
        Assertions.assertEquals(comment, comment1);
        verify(comment1, times(1)).setAuthor(any(User.class));
    }

    @Test
    void saveComment_Should_Assign_Date_Of_Creation_To_Comment() {
        //given
        given(commentsRepository.save(comment1)).willReturn(comment1);
        given(securityContext.getAuthentication()).willReturn(authentication);

        //when
        Comment comment = ticketsService.saveComment(comment1);

        //then
        Assertions.assertEquals(comment, comment1);
        verify(comment1, times(1)).setCreated(any(LocalDateTime.class));
    }

    @Test
    void saveComment_Should_Save_Comment_In_Comment_Repository() {
        //given
        given(commentsRepository.save(comment1)).willReturn(comment1);
        given(securityContext.getAuthentication()).willReturn(authentication);

        //when
        Comment comment = ticketsService.saveComment(comment1);

        //then
        Assertions.assertEquals(comment, comment1);
        verify(commentsRepository, times(1)).save(comment1);
    }

}