package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.service;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config.ApplicationConfigTest;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.repository.UsersRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import(ApplicationConfigTest.class)
class UsersServiceImplTest {
    @Mock
    private UsersRepository usersRepository;
    @Mock
    private User testUser1;
    @Mock
    private Page<User> usersPage;
    @Mock
    private Pageable pageable;
    @Mock
    private PasswordEncoder encoder;
    private UsersService usersService;
    private AutoCloseable autoCloseable;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);

        usersRepository = mock(UsersRepository.class);
        encoder = mock(PasswordEncoder.class);

        usersService = new UsersServiceImpl(usersRepository, encoder);
        testUser1 = mock(User.class);
        usersPage = mock(Page.class);
        pageable = mock(Pageable.class);

    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void findAll_Return_Page_Of_Users() {
        //given
        given(usersRepository.findAll(any(Pageable.class))).willReturn(usersPage);

        //when
        Assertions.assertEquals(usersPage, usersService.findAll(pageable));

        //then
        verify(usersRepository, times(1)).findAll(any(Pageable.class));
    }

    @Test
    void findByName_Return_Page_Of_Users() {
        //given
        given(usersRepository.findByFirstNameContainingOrLastNameContainingIgnoreCase(
                any(String.class),any(String.class), any(Pageable.class))).willReturn(usersPage);

        //when
        Assertions.assertEquals(usersPage, usersService.findByName("", pageable));

        //then
        verify(usersRepository, times(1))
                .findByFirstNameContainingOrLastNameContainingIgnoreCase(
                        any(String.class), any(String.class), any(Pageable.class));
    }


    @Test
    void findById_Return_User_With_Id_1() {
        //given
        int testId = 1;
        given(usersRepository.findById(testId)).willReturn(Optional.of(testUser1));

        //when
        Assertions.assertEquals(usersService.findById(testId), testUser1);

        //then
        verify(usersRepository, times(1)).findById(testId);
    }

    @Test
    void findById_Return_Null_When_Object_Not_Exist() {
        //given
        int testId = 3;
        given(usersRepository.findById(any())).willReturn(Optional.<User>empty());

        //when
        Assertions.assertEquals(usersService.findById(testId), null);

        //then
        verify(usersRepository, times(1)).findById(testId);
    }

    @Test
    void findByUsername_Return_User_By_Username() {
        //given
        String username = "anyUsername";
        given(usersRepository.findByUsername(any())).willReturn(Optional.of(testUser1));

        //when
        Assertions.assertEquals(usersService.findByUsername(username), testUser1);

        //then
        verify(usersRepository, times(1)).findByUsername(username);
    }

    @Test
    void findByUsername_Returns_Null_When_User_Is_Not_Found() {
        //given
        String testUsername = "NotExistingUsername";
        given(usersRepository.findByUsername(testUsername)).willReturn(Optional.<User>empty());

        //when
        User user = usersService.findByUsername(testUsername);
        Assertions.assertNull(user);

        //then
        verify(usersRepository, times(1)).findByUsername(testUsername);
    }

    @Test
    void save_Will_Save_User_In_Database() {
        //given
        given(usersRepository.save(testUser1)).willReturn(testUser1);
        given(usersRepository.findById(testUser1.getId())).willReturn(Optional.of(testUser1));

        //when
        Assertions.assertEquals(usersService.save(testUser1), testUser1);

        //then
        verify(usersRepository, times(1)).save(testUser1);
    }

    @Test
    void deleteAll_Will_Delete_All_Users() {
        //given

        //when
        Assertions.assertTrue(usersService.deleteAll());

        //then
        verify(usersRepository, times(1)).deleteAll();
    }

    @Test
    void deleteById_Delete_Users_With_Id() {
        //given
        int testId = 1;

        //when
        Assertions.assertTrue(usersService.deleteById(testId));

        //then
        verify(usersRepository, times(1)).deleteById(testId);
    }

    @Test
    void deleteById_With_Not_Existing_Id_Will_Not_Throw() {
        //given
        int testId = 100;

        //when
        Assertions.assertTrue(usersService.deleteById(testId));

        //then
        verify(usersRepository, times(1)).deleteById(testId);
    }

    @Test
    void loadUserByUsername_Will_Find_User() {
        //given
        User testUser = new User(1, "test", "test", true, "ROLE_ADMIN", "test", "test", "test", "+48777777777", "test@test");
        given(usersRepository.findByUsername(any())).willReturn(Optional.of(testUser));

        //when
        UserDetails userDetails = usersService.loadUserByUsername(testUser.getUsername());

        String roles = userDetails.getAuthorities().stream()
                .map(n -> String.valueOf(n))
                .collect(Collectors.joining());

        //then
        Assertions.assertEquals(userDetails.getUsername(), testUser.getUsername());
        Assertions.assertEquals(userDetails.getPassword(), testUser.getPassword());
        Assertions.assertEquals(roles, testUser.getRoles());
        Assertions.assertEquals(userDetails.isEnabled(), testUser.isActive());

        verify(usersRepository, times(1)).findByUsername(any());
    }

    @Test
    void loadUserByUsername_Will_Throw_When_User_Not_Exist() {
        //given
        String testUsername = testUser1.getUsername();
        given(usersRepository.findByUsername(testUsername)).willReturn(Optional.<User>empty());

        //when
        Throwable exception = Assertions.assertThrows(UsernameNotFoundException.class, () -> usersService.loadUserByUsername(testUsername));
        Assertions.assertEquals("User not found: " + testUsername, exception.getMessage());

        //then
        verify(usersRepository, times(1)).findByUsername(testUsername);
    }

}
