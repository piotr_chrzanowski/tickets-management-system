package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.dto;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config.ApplicationConfigTest;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import(ApplicationConfigTest.class)
public class UserInfoDTOTest {
    private UserInfoDTO userInfoDTO;
    private User user;

    @Test
    public void insertEntityData_Should_Insert_Entity_Data_To_DTO() {
        //given
        user = new User(1, "test1", "test1", true, "ROLE_USER,ROLE_ADMIN", "FirstNameUser1", "LastNameUser1", "TestDepartment1", "+48777777777", "test1@test.com");
        userInfoDTO = new UserInfoDTO();

        //when
        userInfoDTO.insertEntityData(user);

        //then
        Assertions.assertEquals(user.getId(), userInfoDTO.getId());
        Assertions.assertEquals(user.getEmail(), userInfoDTO.getEmail());
        Assertions.assertEquals(user.getDepartment(), userInfoDTO.getDepartment());
        Assertions.assertEquals(user.getFirstName(), userInfoDTO.getFirstName());
        Assertions.assertEquals(user.getLastName(), userInfoDTO.getLastName());
        Assertions.assertEquals(user.getRoles(), userInfoDTO.getRoles());
        Assertions.assertEquals(user.getPhone(), userInfoDTO.getPhone());
    }

    @Test
    public void insertDtoData_Should_Insert_DTO_Data_To_Entity() {
        //given
        user = new User();
        userInfoDTO = new UserInfoDTO(1, "test", "test", "department", "+4811111111", "test@testmail.com", "ROLE_ADMIN");

        //when
        userInfoDTO.insertDtoData(user);

        //then
        Assertions.assertEquals(userInfoDTO.getId(), user.getId());
        Assertions.assertEquals(userInfoDTO.getEmail(), user.getEmail());
        Assertions.assertEquals(userInfoDTO.getDepartment(), user.getDepartment());
        Assertions.assertEquals(userInfoDTO.getFirstName(), user.getFirstName());
        Assertions.assertEquals(userInfoDTO.getLastName(), user.getLastName());
        Assertions.assertEquals(userInfoDTO.getRoles(), user.getRoles());
        Assertions.assertEquals(userInfoDTO.getPhone(), user.getPhone());
    }
}
