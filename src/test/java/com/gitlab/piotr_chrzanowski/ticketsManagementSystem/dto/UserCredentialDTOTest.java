package com.gitlab.piotr_chrzanowski.ticketsManagementSystem.dto;

import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.config.ApplicationConfigTest;
import com.gitlab.piotr_chrzanowski.ticketsManagementSystem.entity.User;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import(ApplicationConfigTest.class)
public class UserCredentialDTOTest {

    private UserCredentialDTO userCredentialDTO;
    private User user;

    @Test
    public void insertEntityData_Should_Insert_Entity_Data_To_DTO() {
        //given
        user = new User(1, "test1", "test1", true, "ROLE_USER,ROLE_ADMIN", "FirstNameUser1", "LastNameUser1", "TestDepartment1", "+48777777777", "test1@test.com");
        userCredentialDTO = new UserCredentialDTO();

        //when
        userCredentialDTO.insertEntityData(user);

        //then
        Assertions.assertEquals(user.getId(), userCredentialDTO.getId());
        Assertions.assertEquals(user.getUsername(), userCredentialDTO.getUsername());
        Assertions.assertEquals(user.getPassword(), userCredentialDTO.getPassword());
    }

    @Test
    public void insertDtoData_Should_Insert_DTO_Data_To_Entity() {
        //given
        user = new User();
        userCredentialDTO = new UserCredentialDTO(1, "test", "test_password");

        //when
        userCredentialDTO.insertDTOData(user);

        //then
        Assertions.assertEquals(userCredentialDTO.getId(), user.getId());
        Assertions.assertEquals(userCredentialDTO.getPassword(), user.getPassword());
        Assertions.assertEquals(userCredentialDTO.getUsername(), user.getUsername());
    }
}

