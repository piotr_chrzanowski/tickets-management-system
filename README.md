# Tickets Management System

## What is it?

<b>Ticket Management System</b> was created in <b>Java</b>, <b>Spring Boot</b> and <b>MySQL</b> database support. This system provides the capability to create, list, update and delete tickets. Every
ticket has description and a comment section which can help with solving the requests.
To access the system every user must log in(<b>authentication</b>) and could have different roles and privileges(<b>authorization</b>). Users also have the ability to watch statistics data on the charts.

![Alt text](doc/list.png?raw=true "Ticket List")

<p float="left">  
    <img src="doc/user.png" width="100" />
    <img src="doc/stat.png" width="100" />
    <img src="doc/login.png" width="100" />
</p>


## Features
- <b> Tickets and users management</b>
- <b> Database authentication</b>
- <b> Different database routing based on user type</b>
- <b> Visualization data on the charts</b>
- <b> Authorization</b>
- <b> Demo User</b>


## Technologies
- <b>Java 17</b>
- <b>Spring Boot</b>
- <b>Hibernate</b>
- <b>MySql and H2 Database</b>
- <b>Maven</b>
- <b>JUnit, Mockito</b>

## How to Build
####  Maven build
```
$ mvn clean 
$ mvn compile
```


## Licence
This project is licensed under the terms of the **MIT** license.

